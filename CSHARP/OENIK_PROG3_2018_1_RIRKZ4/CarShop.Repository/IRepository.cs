﻿// <copyright file="IRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace CarShop.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// The repository interface
    /// </summary>
    /// <typeparam name="T">what kind of repository it is</typeparam>
    public interface IRepository<T>
    {
        /// <summary>
        /// Gets the table content to string
        /// </summary>
        /// <returns>string of table cantent</returns>
        StringBuilder TablaTartalom { get; }

        /// <summary>
        /// Gets everything as IQueryable
        /// </summary>
        /// <returns>database aas IQueryable</returns>
        IQueryable<T> All { get; }

        /// <summary>
        /// Remove one elemnt
        /// </summary>
        /// <param name="id">the elemnt id</param>
        /// <returns>True if succesful</returns>
        bool Remove(int id);
    }
}
