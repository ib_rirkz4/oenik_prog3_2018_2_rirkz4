﻿// <copyright file="ModellExtraKapcsoloRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace CarShop.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using CarShop.Data;

    /// <summary>
    /// modellekExtraKapcsolo tábla reprezentációja
    /// </summary>
    public class ModellExtraKapcsoloRepository : IRepository<modellExtraKapcsolo>
    {
        /// <summary>
        /// ModellekExtrakapcsolok tábla reprezentációja
        /// </summary>
        private readonly List<modellExtraKapcsolo> modellekExtraKapcsolok;

        /// <summary>
        /// Initializes a new instance of the <see cref="ModellExtraKapcsoloRepository"/> class.
        /// </summary>
        public ModellExtraKapcsoloRepository()
        {
            this.modellekExtraKapcsolok = AdatbazisElero.ModellekExtraKapcsolo;
        }

        /// <summary>
        /// Gets kapcsolo content
        /// </summary>
        /// <returns>stringként a tábla tartalma</returns>
        public StringBuilder TablaTartalom
        {
            get
            {
                List<modellExtraKapcsolo> temp = this.All.ToList();
                StringBuilder sb = new StringBuilder();
                foreach (var item in temp)
                {
                    sb.AppendLine("-* ID: " + item.Id + " | MODELLID: " + item.modell_id + " | EXTRAID: " + item.extra_id);
                }

                return sb;
            }
        }

        /// <summary>
        ///  Gets modellek content
        /// </summary>
        /// <returns>ModellekExtraKapcsoló tábla Queriableként</returns>
        public virtual IQueryable<modellExtraKapcsolo> All => this.modellekExtraKapcsolok.AsQueryable();

        /// <summary>
        /// Egy rekord törlése a táblából
        /// </summary>
        /// <param name="id">modellekextrakapcsolo id-ja</param>
        /// <returns>True ha sikeres</returns>
        public bool Remove(int id)
        {
            List<modellExtraKapcsolo> mek = this.modellekExtraKapcsolok.Where(t => t.Id == id).Select(x => x).ToList();
            if (mek.Count > 0)
            {
                AdatbazisElero.DeleteExtraKapcsolo(mek[0]);
                AdatbazisElero.SaveChanges();
                return true;
            }

            return false;
        }

        /// <summary>
        /// Új kapcsoló hozzáadása
        /// </summary>
        /// <param name="extraid">Kapcsolóhoz tartozó extra ID-ja</param>
        /// <param name="modellid">Kapcsolóhoz tartozó modell ID-ja</param>
        /// <returns>True ha sikeres</returns>
        public bool Hozzoad(int extraid, int modellid)
        {
            modellExtraKapcsolo mek = new modellExtraKapcsolo()
            {
                extra_id = extraid,
                modell_id = modellid
            };
            AdatbazisElero.AddExtraKapcsolo(mek);
            AdatbazisElero.SaveChanges();
            return true;
        }

        /// <summary>
        /// Egy rekord frissítése a kapcsolók táblában
        /// </summary>
        /// <param name="id">kapcsoló ID-ja</param>
        /// <param name="extraid">Kapcsolóhoz tartozó extra ID-ja</param>
        /// <param name="modellid">Kapcsolóhoz tartozó modell ID-ja</param>
        /// <returns>True ha sikeres</returns>
        public bool Frissit(int id, int extraid, int modellid)
        {
            List<modellExtraKapcsolo> mek = this.modellekExtraKapcsolok.Where(t => t.Id == id).Select(x => x).ToList();
            if (mek.Count > 0)
            {
                mek[0].extra_id = extraid;
                mek[0].modell_id = modellid;
                AdatbazisElero.SaveChanges();
                return true;
            }

            return false;
        }
    }
}
