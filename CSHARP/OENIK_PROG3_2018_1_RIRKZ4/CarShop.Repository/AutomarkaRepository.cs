﻿// <copyright file="AutomarkaRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
namespace CarShop.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using CarShop.Data;

    /// <summary>
    /// Reprezentation of automarka repository
    /// </summary>
    public class AutomarkaRepository : IRepository<automarka>
    {
        /// <summary>
        /// A márkák reprezentálása
        /// </summary>
        private readonly List<automarka> automarkak;

        /// <summary>
        /// Initializes a new instance of the <see cref="AutomarkaRepository"/> class.
        /// </summary>
        /// <returns>márkák</returns>
        public AutomarkaRepository()
        {
            this.automarkak = AdatbazisElero.Automarkak;
        }

        /// <summary>
        /// Gets automarka content
        /// </summary>
        /// <returns>Tábla tartalma stringként</returns>
        public StringBuilder TablaTartalom
        {
            get
            {
                List<automarka> temp = this.All.ToList();
                StringBuilder builder = new StringBuilder();
                foreach (var x in temp)
                {
                    builder.AppendLine("-* ID: " + x.Id + " | NÉV: " + x.Nev + " | ORSZÁG: " + x.OrszagNev + " | ÉVES BEVÉTEL: " + x.EvesForgalom + " | ALAPÍTÁS ÉVE: " + x.AlapitasEve);
                }

                return builder;
            }
        }

        /// <summary>
        /// Gets the automarka content
        /// </summary>
        /// <returns>márkák</returns>
        public virtual IQueryable<automarka> All => this.automarkak.AsQueryable();

        /// <summary>
        /// Egy elem hozzáadása az automárka táblához
        /// int ID,string Nev,string OrszagNev, DateTime AlapitasEve, int EvesForgalom
        /// </summary>
        /// <param name="nev">Automarka nev</param>
        /// <param name="orszagnev">Alapítási ország neve</param>
        /// <param name="alapitaseve">Alapítás éve</param>
        /// <param name="evesforgalom">Éves forgalom</param>
        /// <returns>True sikernél</returns>
        public bool HozzaAd(string nev, string orszagnev, DateTime alapitaseve, int evesforgalom)
        {
            automarka auto = new automarka()
            {
                Nev = nev,
                OrszagNev = orszagnev,
                AlapitasEve = alapitaseve,
                EvesForgalom = evesforgalom
            };
            AdatbazisElero.AddAutomarka(auto);
            AdatbazisElero.SaveChanges();
            return true;
        }

        /// <summary>
        /// Egy elem törlése az automarka táblából
        /// int ID,string Nev,string OrszagNev, DateTime AlapitasEve, int EvesForgalom
        /// </summary>
        /// <param name="id">Egy adott elem ID-ja</param>
        /// <returns>True sikernél</returns>
        public bool Remove(int id)
        {
            List<automarka> auto = this.automarkak.Where(t => t.Id == id).Select(x => x).ToList();
            if (auto.Count > 0)
            {
                AdatbazisElero.DeleteAutomarka(auto[0]);
                AdatbazisElero.SaveChanges();
                return true;
            }

            return false;
        }

        /// <summary>
        /// Egy elem frissítése az automárka táblához
        /// int ID,string Nev,string OrszagNev, DateTime AlapitasEve, int EvesForgalom
        /// </summary>
        /// <param name="id">Automarka ID-je</param>
        /// <param name="nev">Automarka nev</param>
        /// <param name="orszagnev">Alapítási ország neve</param>
        /// <param name="alapitaseve">Alapítás éve</param>
        /// <param name="evesforgalom">Éves forgalom</param>
        /// <returns>True sikernél</returns>
        public bool Frissit(int id, string nev, string orszagnev, DateTime alapitaseve, int evesforgalom)
        {
            List<automarka> autok = this.automarkak.Where(t => t.Id == id).Select(x => x).ToList();
            if (autok.Count > 0)
            {
                automarka auto = autok[0];
                auto.Nev = nev;
                auto.OrszagNev = orszagnev;
                auto.AlapitasEve = alapitaseve;
                auto.EvesForgalom = evesforgalom;
                AdatbazisElero.SaveChanges();
                return true;
            }

            return false;
        }

        /// <summary>
        /// Megvizsgálja létezik-e egy adott márka
        /// int ID,string Nev,string OrszagNev, DateTime AlapitasEve, int EvesForgalom
        /// </summary>
        /// <param name="id">Automarka ID-je</param>
        /// /// <returns>True sikernél</returns>
        public bool AutomarkaLetezikE(int id)
        {
            return this.All.Any(x => x.Id == id);
        }
    }
}
