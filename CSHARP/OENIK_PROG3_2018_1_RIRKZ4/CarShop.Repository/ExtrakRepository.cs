﻿// <copyright file="ExtrakRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace CarShop.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using CarShop.Data;

    /// <summary>
    /// Representation of extrak repository
    /// </summary>
    public class ExtrakRepository : IRepository<extrak>
    {
        /// <summary>
        /// Extrák táblát reprezentálja
        /// </summary>
        private readonly List<extrak> extrak;

        /// <summary>
        /// Initializes a new instance of the <see cref="ExtrakRepository"/> class.
        /// </summary>
        public ExtrakRepository()
        {
            this.extrak = AdatbazisElero.Extrak;
        }

        /// <summary>
        /// Gets extra content
        /// </summary>
        /// <returns>Stringként az extra tábla tartalma</returns>
        public StringBuilder TablaTartalom
        {
            get
            {
                List<extrak> temp = this.All.ToList();
                StringBuilder builder = new StringBuilder();
                foreach (var x in temp)
                {
                    builder.AppendLine("-* ID: " + x.Id + " | NÉV: " + x.nev + " | KATEGÓRIA: " + x.kategorianev + " | SZÍN: " + x.szin + " | ÁR: " + x.ar + " | TÖBBSZÖR HASZNÁLHATÓ: " + x.tobbszor_hasznalt_e);
                }

                return builder;
            }
        }

        /// <summary>
        /// Gets the extras
        /// </summary>
        /// <returns>extrák</returns>
        public virtual IQueryable<extrak> All => this.extrak.AsQueryable<extrak>();

        /// <summary>
        /// Törli az extrák egy elemét
        /// </summary>
        /// <param name="id">Törelndő extra ID-ja</param>
        /// <returns>extrák</returns>
        public bool Remove(int id)
        {
            List<extrak> extra = this.extrak.Where(t => t.Id == id).Select(x => x).ToList();
            if (extra.Count > 0)
            {
                AdatbazisElero.DeleteExtra(extra[0]);
                AdatbazisElero.SaveChanges();
                return true;
            }

            return false;
        }

        // int ID, string kategorianev, string nev, int ar, string szin, bool tobbszor_hasznalhato_e

        /// <summary>
        /// Egy ele hozzáadása az etrák táblához
        /// </summary>
        /// <param name="kategorianev">Hozzáadott extra kategóriájának neve</param>
        /// <param name="nev">Az extra neve</param>
        /// <param name="ar">az extra ára</param>
        /// <param name="szin">Az extra színe</param>
        /// <param name="tobbszor_Hasznalhato_E">Többször használható-e az extra</param>
        /// <returns>True ha sikeres volt</returns>
        public bool HozzaAd(string kategorianev, string nev, int ar, string szin, bool tobbszor_Hasznalhato_E)
        {
            extrak extra = new extrak()
            {
                kategorianev = kategorianev,
                nev = nev,
                ar = ar,
                szin = szin,
                tobbszor_hasznalt_e = tobbszor_Hasznalhato_E
            };
            AdatbazisElero.AddExtra(extra);
            AdatbazisElero.SaveChanges();
            return true;
        }

        /// <summary>
        /// Egy ele hozzáadása az etrák táblához
        /// </summary>
        /// <param name="id" >A frisítendő elem ID-ja</param>
        /// <param name="kategorianev">Frissítendő extra kategóriájának neve</param>
        /// <param name="nev">Az extra neve</param>
        /// <param name="ar">az extra ára</param>
        /// <param name="szin">Az extra színe</param>
        /// <param name="tobbszor_Hasznalhato_E">Többször használható-e az extra</param>
        /// <returns>True ha sikeres volt</returns>
        public bool Frissit(int id, string kategorianev, string nev, int ar, string szin, bool tobbszor_Hasznalhato_E)
        {
            List<extrak> extrak = this.extrak.Where(t => t.Id == id).Select(x => x).ToList();
            if (extrak.Count > 0)
            {
                extrak[0].kategorianev = kategorianev;
                extrak[0].nev = nev;
                extrak[0].ar = ar;
                extrak[0].szin = szin;
                extrak[0].tobbszor_hasznalt_e = tobbszor_Hasznalhato_E;
                AdatbazisElero.SaveChanges();
                return true;
            }

            return false;
        }

        /// <summary>
        /// Megvizsgálja létezik-e egy adott extra
        /// </summary>
        /// <param name="id">extra ID-ja</param>
        /// <returns>True ha létezik az extra</returns>
        public bool ExtraletezikE(int id)
        {
            return this.All.Any(t => t.Id == id);
        }
    }
}
