﻿// <copyright file="ModellekRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace CarShop.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using CarShop.Data;

    /// <summary>
    /// Representation of the modellek repository
    /// </summary>
    public class ModellekRepository : IRepository<modellek>
    {
        /// <summary>
        /// Modellek tábla reprezentációja
        /// </summary>
        private readonly List<modellek> modellek;

        /// <summary>
        /// Initializes a new instance of the <see cref="ModellekRepository"/> class.
        /// </summary>
        public ModellekRepository()
        {
            this.modellek = AdatbazisElero.Modellek;
        }

        /// <summary>
        /// Gets modellek content
        /// </summary>
        /// <returns>Stringként a modellek táblát</returns>
        public StringBuilder TablaTartalom
        {
            get
            {
                List<modellek> temp = this.All.ToList();
                StringBuilder builder = new StringBuilder();
                foreach (var item in temp)
                {
                    builder.AppendLine("-* ID: " + item.Id + " | NÉV: " + item.név + " | MÁRKA_ID: " + item.marka_id + " | LÓERŐ: " + item.loero + " | ÁR: " + item.alapar + " | MOTOR_TÉRFOGAT: " + item.motorterfogat + " |  MEGJELENÉS: " + item.megjelenes_napja);
                }

                return builder;
            }
        }

        /// <summary>
        /// Gets modellek content
        /// </summary>
        /// <returns>Modellek</returns>
        public virtual IQueryable<modellek> All => this.modellek.AsQueryable();

        /// <summary>
        /// Egy elem törlése a modellek táblából
        /// </summary>
        /// <param name="id">a tödelndő modell ID-ja</param>
        /// <returns>true ha sikeres</returns>
        public bool Remove(int id)
        {
            List<modellek> modell = this.modellek.Where(t => t.Id == id).Select(x => x).ToList();
            if (modell.Count > 0)
            {
                AdatbazisElero.DeleteModell(modell[0]);
                AdatbazisElero.SaveChanges();
                return true;
            }

            return false;
        }

        /// <summary>
        /// ELem hozzáadása a modellek táblához
        /// </summary>
        /// <param name="markaid">A hozzáadandó modell márkaDID-ja</param>
        /// <param name="megjelenesiido">A hozzáadandó modell megjelenési ideje</param>
        /// <param name="mototerfogat">A hozzáadandó modell motortérfogata</param>
        /// <param name="loero">A hozzáadandó modell lóereje</param>
        /// <param name="alapar">A hozzáadandó modell alapára</param>
        /// <param name="nev">A hozzáadandó modell neve</param>
        /// <returns>True ha siekres</returns>
        public bool HozzaAd(int markaid, DateTime megjelenesiido, int mototerfogat, int loero, int alapar, string nev)
        {
            modellek modell = new modellek()
            {
                marka_id = markaid,
                megjelenes_napja = megjelenesiido,
                motorterfogat = mototerfogat,
                loero = loero,
                alapar = alapar,
                név = nev
            };
            AdatbazisElero.AddMarka(modell);
            AdatbazisElero.SaveChanges();
            return true;
        }

        /// <summary>
        /// Egy adott modell frisítése
        /// </summary>
        /// <param name="id">Frissítendő modell ID-ja</param>
        /// <param name="markaid">Frissítendő modell marka ID-ja</param>
        /// <param name="megjelenesiido">Frissítendő modell megjelenési ideje</param>
        /// <param name="mototerfogat">Frissítendő modell motortérfogata</param>
        /// <param name="loero">Frissítendő modell lóereje</param>
        /// <param name="alapar">Frissítendő modell alapára</param>
        /// <param name="nev">Frissítendő modell neve</param>
        /// <returns>True ha sikeres</returns>
        public bool Frissit(int id, int markaid, DateTime megjelenesiido, int mototerfogat, int loero, int alapar, string nev)
        {
            List<modellek> modellek = this.modellek.Where(t => t.Id == id).Select(x => x).ToList();
            if (modellek.Count > 0)
            {
                modellek[0].marka_id = markaid;
                modellek[0].megjelenes_napja = megjelenesiido;
                modellek[0].motorterfogat = mototerfogat;
                modellek[0].loero = loero;
                modellek[0].alapar = alapar;
                modellek[0].név = nev;
                AdatbazisElero.SaveChanges();
                return true;
            }

            return false;
        }

        /// <summary>
        /// Visszadja, hogy adott ID-jú modell létezik-e
        /// </summary>
        /// <param name="id">modell id-ja</param>
        /// <returns>True ha létezik iylen modell</returns>
        public bool ModellLetezikE(int id)
        {
            return this.All.Any(t => t.Id == id);
        }
    }
}
