﻿// <copyright file="OsszefogoRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace CarShop.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using CarShop.Data;

    /// <summary>
    /// Az összes repository-t összefogó osztály
    /// </summary>
    public class OsszefogoRepository
    {
        /// <summary>
        /// automákák repository-ja
        /// </summary>
        private readonly AutomarkaRepository aR;

        /// <summary>
        /// extrák repository-ja
        /// </summary>
        private readonly ExtrakRepository eR;

        /// <summary>
        /// modellek repository-ja
        /// </summary>
        private readonly ModellekRepository mR;

        /// <summary>
        /// Kapcsolók repository-ja
        /// </summary>
        private readonly ModellExtraKapcsoloRepository mEKR;

        /// <summary>
        /// Initializes a new instance of the <see cref="OsszefogoRepository"/> class.
        /// </summary>
        public OsszefogoRepository()
        {
            this.aR = new AutomarkaRepository();
            this.eR = new ExtrakRepository();
            this.mR = new ModellekRepository();
            this.mEKR = new ModellExtraKapcsoloRepository();
        }

        /// <summary>
        /// Gets the AutomarkaRepository
        /// </summary>
        public virtual AutomarkaRepository AR { get => this.aR; }

        /// <summary>
        /// Gets the ExtraRepository
        /// </summary>
        public virtual ExtrakRepository ER { get => this.eR; }

        /// <summary>
        /// Gets the Modellekrepository
        /// </summary>
        public virtual ModellekRepository MR { get => this.mR; }

        /// <summary>
        /// Gets the ModellExtraKapcsoloRepository
        /// </summary>
        public virtual ModellExtraKapcsoloRepository Mekr { get => this.mEKR; }

        /// <summary>
        /// Gets the list of the names of the tables
        /// </summary>
        /// <returns>List of table names</returns>
        public List<string> TablaNevek()
        {
            return AdatbazisElero.TablaNevek;
        }
    }
}
