﻿// <copyright file="ILogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace CarShop.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using CarShop.Data;

    /// <summary>
    /// The Logicinterface
    /// </summary>
    public interface ILogic
    {
        /// <summary>
        /// Gets automarka content
        /// </summary>
        /// <returns>Tábla tartalma stringként</returns>
        StringBuilder AutomarkaTartalom { get; }

        /// <summary>
        /// Gets extrak content
        /// </summary>
        /// <returns>Stringként az extra tábla tartalma</returns>
        StringBuilder ExtraTartalom { get; }

        /// <summary>
        /// Gets modellek content
        /// </summary>
        /// <returns>Stringként a modellek táblát</returns>
        StringBuilder ModellTartalom { get; }

        /// <summary>
        /// Gets kapcsolo tatalom
        /// </summary>
        /// <returns>stringként a tábla tartalma</returns>
        StringBuilder Kapcsolotartalom { get; }

        /// <summary>
        /// Vissza adja az összes tábla nevét
        /// </summary>
        /// <returns>stringet listája a táblák neveivel</returns>
        List<string> MindenTabla();

        /// <summary>
        /// Egy elem hozzáadása az automárka táblához
        /// int ID,string Nev,string OrszagNev, DateTime AlapitasEve, int EvesForgalom
        /// </summary>
        /// <param name="nev">Automarka nev</param>
        /// <param name="orszagnev">Alapítási ország neve</param>
        /// <param name="alapitaseve">Alapítás éve</param>
        /// <param name="evesforgalom">Éves forgalom</param>
        /// <returns>True sikernél</returns>
        bool AddAutomarka(string nev, string orszagnev, DateTime alapitaseve, int evesforgalom);

        /// <summary>
        /// Egy ele hozzáadása az etrák táblához
        /// </summary>
        /// <param name="kategorianev">Hozzáadott extra kategóriájának neve</param>
        /// <param name="nev">Az extra neve</param>
        /// <param name="ar">az extra ára</param>
        /// <param name="szin">Az extra színe</param>
        /// <param name="tobbszor_Hasznalhato_E">Többször használható-e az extra</param>
        /// <returns>True ha sikeres volt</returns>
        bool AddExtra(string kategorianev, string nev, int ar, string szin, bool tobbszor_Hasznalhato_E);

        /// <summary>
        /// ELem hozzáadása a modellek táblához
        /// </summary>
        /// <param name="markaid">A hozzáadandó modell márkaDID-ja</param>
        /// <param name="megjelenesiido">A hozzáadandó modell megjelenési ideje</param>
        /// <param name="mototerfogat">A hozzáadandó modell motortérfogata</param>
        /// <param name="loero">A hozzáadandó modell lóereje</param>
        /// <param name="alapar">A hozzáadandó modell alapára</param>
        /// <param name="nev">A hozzáadandó modell neve</param>
        /// <returns>True ha siekres</returns>
        bool AddModell(int markaid, DateTime megjelenesiido, int mototerfogat, int loero, int alapar, string nev);

        /// <summary>
        /// Új kapcsoló hozzáadása
        /// </summary>
        /// <param name="extraid">Kapcsolóhoz tartozó extra ID-ja</param>
        /// <param name="modellid">Kapcsolóhoz tartozó modell ID-ja</param>
        /// <returns>True ha sikeres</returns>
        bool AddKapcsolo(int extraid, int modellid);

        /// <summary>
        /// Egy elem frissítése az automárka táblához
        /// int ID,string Nev,string OrszagNev, DateTime AlapitasEve, int EvesForgalom
        /// </summary>
        /// <param name="id">Automarka ID-je</param>
        /// <param name="nev">Automarka nev</param>
        /// <param name="orszagnev">Alapítási ország neve</param>
        /// <param name="alapitaseve">Alapítás éve</param>
        /// <param name="evesforgalom">Éves forgalom</param>
        /// <returns>True sikernél</returns>
        bool FrissitAutomarka(int id, string nev, string orszagnev, DateTime alapitaseve, int evesforgalom);

        /// <summary>
        /// Egy ele hozzáadása az etrák táblához
        /// </summary>
        /// <param name="id" >A frisítendő elem ID-ja</param>
        /// <param name="kategorianev">Frissítendő extra kategóriájának neve</param>
        /// <param name="nev">Az extra neve</param>
        /// <param name="ar">az extra ára</param>
        /// <param name="szin">Az extra színe</param>
        /// <param name="tobbszor_Hasznalhato_E">Többször használható-e az extra</param>
        /// <returns>True ha sikeres volt</returns>
        bool FrissitExtra(int id, string kategorianev, string nev, int ar, string szin, bool tobbszor_Hasznalhato_E);

        /// <summary>
        /// Egy adott modell frisítése
        /// </summary>
        /// <param name="id">Frissítendő modell ID-ja</param>
        /// <param name="markaid">Frissítendő modell marka ID-ja</param>
        /// <param name="megjelenesiido">Frissítendő modell megjelenési ideje</param>
        /// <param name="mototerfogat">Frissítendő modell motortérfogata</param>
        /// <param name="loero">Frissítendő modell lóereje</param>
        /// <param name="alapar">Frissítendő modell alapára</param>
        /// <param name="nev">Frissítendő modell neve</param>
        /// <returns>True ha sikeres</returns>
        bool FrissitModell(int id, int markaid, DateTime megjelenesiido, int mototerfogat, int loero, int alapar, string nev);

        /// <summary>
        /// Egy rekord frissítése a kapcsolók táblában
        /// </summary>
        /// <param name="id">kapcsoló ID-ja</param>
        /// <param name="extraid">Kapcsolóhoz tartozó extra ID-ja</param>
        /// <param name="modellid">Kapcsolóhoz tartozó modell ID-ja</param>
        /// <returns>True ha sikeres</returns>
        bool FrissitKapcsolo(int id, int extraid, int modellid);

        /// <summary>
        /// Megvizsgálja létezik-e egy adott márka
        /// int ID,string Nev,string OrszagNev, DateTime AlapitasEve, int EvesForgalom
        /// </summary>
        /// <param name="id">Automarka ID-je</param>
        /// /// <returns>True sikernél</returns>
        bool LetezikEAutomarka(int id);

        /// <summary>
        /// Visszadja, hogy adott ID-jú modell létezik-e
        /// </summary>
        /// <param name="id">modell id-ja</param>
        /// <returns>True ha létezik iylen modell</returns>
        bool LetezikEModell(int id);

        /// <summary>
        /// Megvizsgálja létezik-e egy adott extra
        /// </summary>
        /// <param name="id">extra ID-ja</param>
        /// <returns>True ha létezik az extra</returns>
        bool LetezikEExtra(int id);

        /// <summary>
        /// Egy elem törlése az automarka táblából
        /// int ID,string Nev,string OrszagNev, DateTime AlapitasEve, int EvesForgalom
        /// </summary>
        /// <param name="id">Egy adott elem ID-ja</param>
        /// <returns>True sikernél</returns>
        bool RemoveAutomarka(int id);

        /// <summary>
        /// Egy elem törlése a modellek táblából
        /// </summary>
        /// <param name="id">a tödelndő modell ID-ja</param>
        /// <returns>true ha sikeres</returns>
        bool RemoveModell(int id);

        /// <summary>
        /// Törli az extrák egy elemét
        /// </summary>
        /// <param name="id">Törelndő extra ID-ja</param>
        /// <returns>extrák</returns>
        bool RemoveExtra(int id);

        /// <summary>
        /// Egy rekord törlése a táblából
        /// </summary>
        /// <param name="id">modellekextrakapcsolo id-ja</param>
        /// <returns>True ha sikeres</returns>
        bool RemoveKapcsolo(int id);
    }
}
