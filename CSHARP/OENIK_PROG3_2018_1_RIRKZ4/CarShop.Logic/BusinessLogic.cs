﻿// <copyright file="BusinessLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace CarShop.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using CarShop.Data;
    using CarShop.Repository;

    /// <summary>
    /// the business logic class
    /// </summary>
    public class BusinessLogic : ILogic
    {
        private readonly OsszefogoRepository osszefogRep;

        /// <summary>
        /// Initializes a new instance of the <see cref="BusinessLogic"/> class.
        /// </summary>
        public BusinessLogic()
        {
            this.osszefogRep = new OsszefogoRepository();
        }

        /// <summary>
        /// Gets the osszefogo repo
        /// </summary>
        public virtual OsszefogoRepository Osszefogo
        {
            get
            {
                return this.osszefogRep;
            }
        }

        /// <summary>
        /// Gets visszaadja a modellek táblát stringként
        /// </summary>
        /// <returns>Stringként a modellek táblát</returns>
        public virtual StringBuilder ModellTartalom => this.osszefogRep.MR.TablaTartalom;

        /// <summary>
        /// Gets stringé konvertálja a tábla tartalmát
        /// </summary>
        /// <returns>stringként a tábla tartalma</returns>
        public virtual StringBuilder Kapcsolotartalom => this.osszefogRep.Mekr.TablaTartalom;

        /// <summary>
        /// Gets stringé alakítja az egész tábla tartalmát
        /// </summary>
        /// <returns>Stringként az extra tábla tartalma</returns>
        public virtual StringBuilder ExtraTartalom => this.osszefogRep.ER.TablaTartalom;

        /// <summary>
        /// Gets vissza adja a tábla tartalmát egy stringként
        /// </summary>
        /// <returns>Tábla tartalma stringként</returns>
        public virtual StringBuilder AutomarkaTartalom => this.osszefogRep.AR.TablaTartalom;

        /// <summary>
        /// Gets the id of the brand
        /// </summary>
        /// <param name="marka">The brand name</param>
        /// <returns>The id of the brand</returns>
        public int AutomarkaKeresNevSzerint(string marka)
        {
            return this.osszefogRep.AR.All.Where(t => t.Nev == marka).Select(t => t.Id).FirstOrDefault();
        }

        /// <summary>
        /// Egy elem hozzáadása az automárka táblához
        /// int ID,string Nev,string OrszagNev, DateTime AlapitasEve, int EvesForgalom
        /// </summary>
        /// <param name="nev">Automarka nev</param>
        /// <param name="orszagnev">Alapítási ország neve</param>
        /// <param name="alapitaseve">Alapítás éve</param>
        /// <param name="evesforgalom">Éves forgalom</param>
        /// <returns>True sikernél</returns>
        public virtual bool AddAutomarka(string nev, string orszagnev, DateTime alapitaseve, int evesforgalom)
        {
            try
            {
                return this.osszefogRep.AR.HozzaAd(nev, orszagnev, alapitaseve, evesforgalom);
            }
            catch (Exception e)
            {
                Console.WriteLine("HIBA a hozzáadásnál: " + e);
            }

            return false;
        }

        /// <summary>
        /// Egy ele hozzáadása az etrák táblához
        /// </summary>
        /// <param name="kategorianev">Hozzáadott extra kategóriájának neve</param>
        /// <param name="nev">Az extra neve</param>
        /// <param name="ar">az extra ára</param>
        /// <param name="szin">Az extra színe</param>
        /// <param name="tobbszor_Hasznalhato_E">Többször használható-e az extra</param>
        /// <returns>True ha sikeres volt</returns>
        public virtual bool AddExtra(string kategorianev, string nev, int ar, string szin, bool tobbszor_Hasznalhato_E)
        {
            try
            {
                return this.osszefogRep.ER.HozzaAd(kategorianev, nev, ar, szin, tobbszor_Hasznalhato_E);
            }
            catch (Exception e)
            {
                Console.WriteLine("HIBA a hozzáadásnál: " + e);
            }

            return false;
        }

        /// <summary>
        /// Új kapcsoló hozzáadása
        /// </summary>
        /// <param name="extraid">Kapcsolóhoz tartozó extra ID-ja</param>
        /// <param name="modellid">Kapcsolóhoz tartozó modell ID-ja</param>
        /// <returns>True ha sikeres</returns>
        public virtual bool AddKapcsolo(int extraid, int modellid)
        {
            try
            {
                return this.osszefogRep.Mekr.Hozzoad(extraid, modellid);
            }
            catch (Exception e)
            {
                Console.WriteLine("HIBA a hozzáadásnál: " + e);
            }

            return false;
        }

        /// <summary>
        /// ELem hozzáadása a modellek táblához
        /// </summary>
        /// <param name="markaid">A hozzáadandó modell márkaDID-ja</param>
        /// <param name="megjelenesiido">A hozzáadandó modell megjelenési ideje</param>
        /// <param name="mototerfogat">A hozzáadandó modell motortérfogata</param>
        /// <param name="loero">A hozzáadandó modell lóereje</param>
        /// <param name="alapar">A hozzáadandó modell alapára</param>
        /// <param name="nev">A hozzáadandó modell neve</param>
        /// <returns>True ha siekres</returns>
        public virtual bool AddModell(int markaid, DateTime megjelenesiido, int mototerfogat, int loero, int alapar, string nev)
        {
            try
            {
                return this.osszefogRep.MR.HozzaAd(markaid, megjelenesiido, mototerfogat, loero, alapar, nev);
            }
            catch (Exception e)
            {
                Console.WriteLine("HIBA a hozzáadásnál: " + e);
            }

            return false;
        }

        /// <summary>
        /// Egy elem frissítése az automárka táblához
        /// int ID,string Nev,string OrszagNev, DateTime AlapitasEve, int EvesForgalom
        /// </summary>
        /// <param name="id">Automarka ID-je</param>
        /// <param name="nev">Automarka nev</param>
        /// <param name="orszagnev">Alapítási ország neve</param>
        /// <param name="alapitaseve">Alapítás éve</param>
        /// <param name="evesforgalom">Éves forgalom</param>
        /// <returns>True sikernél</returns>
        public virtual bool FrissitAutomarka(int id, string nev, string orszagnev, DateTime alapitaseve, int evesforgalom)
        {
            try
            {
                return this.osszefogRep.AR.Frissit(id, nev, orszagnev, alapitaseve, evesforgalom);
            }
            catch (Exception e)
            {
                Console.WriteLine("HIBA a frissítésnél: " + e);
            }

            return false;
        }

        /// <summary>
        /// Egy ele hozzáadása az etrák táblához
        /// </summary>
        /// <param name="id" >A frisítendő elem ID-ja</param>
        /// <param name="kategorianev">Frissítendő extra kategóriájának neve</param>
        /// <param name="nev">Az extra neve</param>
        /// <param name="ar">az extra ára</param>
        /// <param name="szin">Az extra színe</param>
        /// <param name="tobbszor_Hasznalhato_E">Többször használható-e az extra</param>
        /// <returns>True ha sikeres volt</returns>
        public bool FrissitExtra(int id, string kategorianev, string nev, int ar, string szin, bool tobbszor_Hasznalhato_E)
        {
            try
            {
                return this.osszefogRep.ER.Frissit(id, kategorianev, nev, ar, szin, tobbszor_Hasznalhato_E);
            }
            catch (Exception e)
            {
                Console.WriteLine("HIBA a frissítésnél: " + e);
            }

            return false;
        }

        /// <summary>
        /// Egy rekord frissítése a kapcsolók táblában
        /// </summary>
        /// <param name="id">kapcsoló ID-ja</param>
        /// <param name="extraid">Kapcsolóhoz tartozó extra ID-ja</param>
        /// <param name="modellid">Kapcsolóhoz tartozó modell ID-ja</param>
        /// <returns>True ha sikeres</returns>
        public bool FrissitKapcsolo(int id, int extraid, int modellid)
        {
            try
            {
                return this.osszefogRep.Mekr.Frissit(id, extraid, modellid);
            }
            catch (Exception e)
            {
                Console.WriteLine("HIBA a frissítésnél: " + e);
            }

            return false;
        }

        /// <summary>
        /// Egy adott modell frisítése
        /// </summary>
        /// <param name="id">Frissítendő modell ID-ja</param>
        /// <param name="markaid">Frissítendő modell marka ID-ja</param>
        /// <param name="megjelenesiido">Frissítendő modell megjelenési ideje</param>
        /// <param name="mototerfogat">Frissítendő modell motortérfogata</param>
        /// <param name="loero">Frissítendő modell lóereje</param>
        /// <param name="alapar">Frissítendő modell alapára</param>
        /// <param name="nev">Frissítendő modell neve</param>
        /// <returns>True ha sikeres</returns>
        public bool FrissitModell(int id, int markaid, DateTime megjelenesiido, int mototerfogat, int loero, int alapar, string nev)
        {
            try
            {
                return this.osszefogRep.MR.Frissit(id, markaid, megjelenesiido, mototerfogat, loero, alapar, nev);
            }
            catch (Exception e)
            {
                Console.WriteLine("HIBA a frissítésnél: " + e);
            }

            return false;
        }

        /// <summary>
        /// Megvizsgálja létezik-e egy adott márka
        /// int ID,string Nev,string OrszagNev, DateTime AlapitasEve, int EvesForgalom
        /// </summary>
        /// <param name="id">Automarka ID-je</param>
        /// /// <returns>True sikernél</returns>
        public bool LetezikEAutomarka(int id)
        {
            return this.osszefogRep.AR.AutomarkaLetezikE(id);
        }

        /// <summary>
        /// Megvizsgálja létezik-e egy adott extra
        /// </summary>
        /// <param name="id">extra ID-ja</param>
        /// <returns>True ha létezik az extra</returns>
        public bool LetezikEExtra(int id)
        {
            return this.osszefogRep.ER.ExtraletezikE(id);
        }

        /// <summary>
        /// Visszadja, hogy adott ID-jú modell létezik-e
        /// </summary>
        /// <param name="id">modell id-ja</param>
        /// <returns>True ha létezik iylen modell</returns>
        public bool LetezikEModell(int id)
        {
            return this.osszefogRep.MR.ModellLetezikE(id);
        }

        /// <summary>
        /// Vissza adja az összes tábla nevét
        /// </summary>
        /// <returns>stringet listája a táblák neveivel</returns>
        public List<string> MindenTabla()
        {
            return this.osszefogRep.TablaNevek();
        }

        /// <summary>
        /// Egy elem törlése az automarka táblából
        /// int ID,string Nev,string OrszagNev, DateTime AlapitasEve, int EvesForgalom
        /// </summary>
        /// <param name="id">Egy adott elem ID-ja</param>
        /// <returns>True sikernél</returns>
        public bool RemoveAutomarka(int id)
        {
            try
            {
                return this.osszefogRep.AR.Remove(id);
            }
            catch (Exception e)
            {
                Console.WriteLine("HIBA a törlésnél: " + e);
            }

            return false;
        }

        /// <summary>
        /// Törli az extrák egy elemét
        /// </summary>
        /// <param name="id">Törelndő extra ID-ja</param>
        /// <returns>True ha sikeres</returns>
        public bool RemoveExtra(int id)
        {
            try
            {
                return this.osszefogRep.ER.Remove(id);
            }
            catch (Exception e)
            {
                Console.WriteLine("HIBA a törlésnél: " + e);
            }

            return false;
        }

        /// <summary>
        /// Egy rekord törlése a táblából
        /// </summary>
        /// <param name="id">modellekextrakapcsolo id-ja</param>
        /// <returns>True ha sikeres</returns>
        public bool RemoveKapcsolo(int id)
        {
            try
            {
                return this.osszefogRep.Mekr.Remove(id);
            }
            catch (Exception e)
            {
                Console.WriteLine("HIBA a törlésnél: " + e);
            }

            return false;
        }

        /// <summary>
        /// Törli a modellek elemét
        /// </summary>
        /// <param name="id">Törelndő extra ID-ja</param>
        /// <returns>True ha siekres</returns>
        public bool RemoveModell(int id)
        {
            try
            {
                return this.osszefogRep.MR.Remove(id);
            }
            catch (Exception e)
            {
                Console.WriteLine("HIBA a törlésnél: " + e);
            }

            return false;
        }

        /// <summary>
        /// Gets all the carsd that brand was made after 1990
        /// </summary>
        /// <returns>List of strings</returns>
        public List<string> CarsAfter1990()
        {
            List<string> choosens = new List<string>();
            var q = from x in this.osszefogRep.AR.All
                    join modell in this.osszefogRep.MR.All on x.Id equals modell.marka_id
                    where x.AlapitasEve > DateTime.Parse("1990-09-09")
                    select new { MarkaNev = x.Nev, ModellNev = modell.név, AlapitasiEv = x.AlapitasEve.ToString() };
            foreach (var item in q)
            {
                choosens.Add(item.MarkaNev + ", " + item.ModellNev + ", " + item.AlapitasiEv);
            }

            return choosens;
        }

        /// <summary>
        /// Gets the full price for each modell (alapár + összes extra ára rajta)
        /// </summary>
        /// <returns>string list with modell name, fullprice </returns>
        public List<string> CarFullPrice()
        {
            List<string> choosens;
            var q = from x in this.osszefogRep.MR.All
                    join kapcsolo in this.osszefogRep.Mekr.All on x.Id equals kapcsolo.modell_id
                    join extra in this.osszefogRep.ER.All on kapcsolo.extra_id equals extra.Id
                    select new { ModellName = x.név, Ar = extra.ar, AlapAr = x.alapar };
            choosens = new List<string>();

            for (int i = 0; i < q.Count(); i++)
            {
                int teljesar = 0;
                string name = q.ToList()[i].ModellName;
                foreach (var item in q)
                {
                    if (name == item.ModellName)
                    {
                        teljesar += (int)item.Ar;
                    }
                }

                if (!choosens.Any(t => t.Remove(t.IndexOf(',')) == name))
                {
                    choosens.Add(name + ", " + teljesar);
                }
            }

            return choosens;
        }

        /// <summary>
        /// Gets all the Német brands
        /// </summary>
        /// <returns>List of string with the Német brands and modells</returns>
        public List<string> NemetAutomarkaESModell()
        {
            List<string> choosen = new List<string>();
            var q = from x in this.osszefogRep.AR.All
                    join modell in this.osszefogRep.MR.All on x.Id equals modell.marka_id
                    where x.OrszagNev == "Német"
                    select new { MarkaNev = x.Nev, ModellNev = modell.név };
            foreach (var item in q)
            {
                choosen.Add(item.MarkaNev + " " + item.ModellNev);
            }

            return choosen;
        }
    }
}
