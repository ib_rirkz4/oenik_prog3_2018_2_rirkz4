﻿CREATE TABLE [dbo].[automarka] (
    [Id]           INT           NOT NULL,
    [Nev]          NVARCHAR (50) NULL,
    [OrszagNev]    NVARCHAR (50) NULL,s
    [AlapitasEve]  DATE          NULL,
    [EvesForgalom] INT           NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC)
);

