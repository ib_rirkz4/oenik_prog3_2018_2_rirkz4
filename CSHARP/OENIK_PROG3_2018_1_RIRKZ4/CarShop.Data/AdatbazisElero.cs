﻿// <copyright file="AdatbazisElero.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace CarShop.Data
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Az osszes adatbázis elérése egy helyről
    /// </summary>
    public static class AdatbazisElero
    {
        /// <summary>
        /// DatabaseEntiti reprezentációja
        /// </summary>
        private static readonly CarShopDataBaseEntities CarShopAllomanyokValue = new CarShopDataBaseEntities();

        /// <summary>
        /// Gets? Visszaadja DataBaseEntitit
        /// </summary>
        public static CarShopDataBaseEntities CarShopAllomanyok
        {
            get
            {
                return CarShopAllomanyokValue;
            }
        }

        /// <summary>
        /// Gets Visszaadja minden tábla nevét
        /// </summary>
        public static List<string> TablaNevek
        {
            get
            {
                return CarShopAllomanyokValue.Database.SqlQuery<string>("SELECT name FROM sys.tables ORDER BY name").ToList();
            }
        }

        /// <summary>
        /// Gets Lsitává alakítja az autómákákat
        /// </summary>
        /// <returns>List of automarka</returns>
        public static List<automarka> Automarkak
        {
            get
            {
                var query = CarShopAllomanyokValue.automarka.Select(x => x);
                return query.ToList();
            }
        }

        /// <summary>
        /// Gets the modellekExtraKapcsolo table
        /// </summary>
        /// <returns>Lost of modellekExtraKapcsolo</returns>
        public static List<modellExtraKapcsolo> ModellekExtraKapcsolo
        {
            get
            {
                var query = CarShopAllomanyokValue.modellExtraKapcsolo.Select(x => x);
                return query.ToList();
            }
        }

        /// <summary>
        /// Gets the extrak table
        /// </summary>
        /// <returns>Lost of extrak</returns>
        public static List<extrak> Extrak
        {
            get
            {
                var query = CarShopAllomanyokValue.extrak.Select(x => x);
                return query.ToList();
            }
        }

        /// <summary>
        /// Gets the modellek table
        /// </summary>
        /// <returns>Lost of modellek</returns>
        public static List<modellek> Modellek
        {
            get
            {
                var query = CarShopAllomanyokValue.modellek.Select(x => x);
                return query.ToList();
            }
        }

        /// <summary>
        ///  Gets add a new elemnt to the database
        /// </summary>
        /// <param name="am">represent the new automarka element</param>
        public static void AddAutomarka(automarka am)
        {
            CarShopAllomanyokValue.automarka.Add(am);
        }

        /// <summary>
        ///  Gets add a new elemnt to the database
        /// </summary>
        /// <param name="am">represent the new extra element</param>
        public static void AddExtra(extrak am)
        {
            CarShopAllomanyokValue.extrak.Add(am);
        }

        /// <summary>
        ///  Gets add a new elemnt to the database
        /// </summary>
        /// <param name="am">represent the new modell element</param>
        public static void AddMarka(modellek am)
        {
            CarShopAllomanyokValue.modellek.Add(am);
        }

        /// <summary>
        ///  Gets add a new elemnt to the database
        /// </summary>
        /// <param name="am">represent the new modellExtraKapcsolo element</param>
        public static void AddExtraKapcsolo(modellExtraKapcsolo am)
        {
            CarShopAllomanyokValue.modellExtraKapcsolo.Add(am);
        }

        /// <summary>
        /// Delete az element from the database
        /// </summary>
        /// <param name="am">represent the elemtn we want to delete from automarka table</param>
        public static void DeleteAutomarka(automarka am)
        {
            CarShopAllomanyokValue.automarka.Remove(am);
        }

        /// <summary>
        /// Delete az element from the database
        /// </summary>
        /// <param name="am">represent the elemtn we want to delete from extrak table</param>
        public static void DeleteExtra(extrak am)
        {
            CarShopAllomanyokValue.extrak.Remove(am);
        }

        /// <summary>
        /// Delete az element from the database
        /// </summary>
        /// <param name="am">represent the elemtn we want to delete from modellek table</param>
        public static void DeleteModell(modellek am)
        {
            CarShopAllomanyokValue.modellek.Remove(am);
        }

        /// <summary>
        /// Delete az element from the database
        /// </summary>
        /// <param name="am">represent the elemtn we want to delete from modellekExtraKapcsolo table</param>
        public static void DeleteExtraKapcsolo(modellExtraKapcsolo am)
        {
            CarShopAllomanyokValue.modellExtraKapcsolo.Remove(am);
        }

        /// <summary>
        /// Gets Elemnti a változásokat
        /// </summary>
        public static void SaveChanges()
        {
            CarShopAllomanyokValue.SaveChanges();
        }
    }
}
