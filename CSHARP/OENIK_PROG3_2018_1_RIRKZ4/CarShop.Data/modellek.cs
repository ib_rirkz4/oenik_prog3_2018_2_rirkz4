//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace CarShop.Data
{
    using System;
    using System.Collections.Generic;
    
    public partial class modellek
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public modellek()
        {
            this.modellExtraKapcsolo = new HashSet<modellExtraKapcsolo>();
        }
    
        public int Id { get; set; }
        public int marka_id { get; set; }
        public Nullable<System.DateTime> megjelenes_napja { get; set; }
        public Nullable<int> motorterfogat { get; set; }
        public Nullable<int> loero { get; set; }
        public Nullable<int> alapar { get; set; }
        public string név { get; set; }
    
        public virtual automarka automarka { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<modellExtraKapcsolo> modellExtraKapcsolo { get; set; }
    }
}
