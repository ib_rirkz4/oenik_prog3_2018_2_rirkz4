﻿CREATE TABLE [dbo].[extrak] (
    [Id]                  INT          PRIMARY KEY IDENTITY  NOT NULL,
    [kategorianev]        NVARCHAR (50) NULL,
    [nev]                 NVARCHAR (50) NULL,
    [ar]                  INT           NULL,
    [szin]                NCHAR (10)    NULL,
    [tobbszor_hasznalt_e] BIT           NULL
);

