﻿CREATE TABLE [dbo].[modellExtraKapcsolo] (
    [Id]        INT  PRIMARY KEY IDENTITY NOT NULL,
    [modell_id] INT NOT NULL,
    [extra_id]  INT NOT NULL,
    CONSTRAINT [FK_modellExtraKapcsolo_Tomodellek] FOREIGN KEY ([modell_id]) REFERENCES [dbo].[modellek] ([Id]),
    CONSTRAINT [FK_modellExtraKapcsolo_Toextrak] FOREIGN KEY ([extra_id]) REFERENCES [dbo].[extrak] ([Id])
);

