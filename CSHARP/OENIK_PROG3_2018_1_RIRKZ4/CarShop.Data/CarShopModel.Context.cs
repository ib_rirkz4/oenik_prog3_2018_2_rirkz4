﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace CarShop.Data
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    
    public partial class CarShopDataBaseEntities : DbContext
    {
        public CarShopDataBaseEntities()
            : base("name=CarShopDataBaseEntities")
        {
        }
    
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            throw new UnintentionalCodeFirstException();
        }
    
        public virtual DbSet<automarka> automarka { get; set; }
        public virtual DbSet<extrak> extrak { get; set; }
        public virtual DbSet<modellek> modellek { get; set; }
        public virtual DbSet<modellExtraKapcsolo> modellExtraKapcsolo { get; set; }
    }
}
