﻿CREATE TABLE [dbo].[modellek] (
    [Id]               INT           PRIMARY KEY IDENTITY NOT NULL,
    [marka_id]         INT          NOT NULL,
    [megjelenes_napja] DATE         NULL,
    [motorterfogat]    INT          NULL,
    [loero]            INT          NULL,
    [alapar]           INT          NULL,
    [név]              VARCHAR (50) NULL,
    CONSTRAINT [FK_modellek_Toautomarka] FOREIGN KEY ([marka_id]) REFERENCES [dbo].[automarka] ([Id])
);

