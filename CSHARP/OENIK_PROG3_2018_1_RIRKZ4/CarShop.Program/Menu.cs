﻿// <copyright file="Menu.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace CarShop.Program
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using CarShop.Logic;

    /// <summary>
    /// The menu that show
    /// </summary>
    public class Menu
    {
        /// <summary>
        /// BuseinessLogic repressentation
        /// </summary>
        private readonly BusinessLogic dbLogic = new BusinessLogic();

        /// <summary>
        /// Make magic, elvégzi a választot műveletet
        /// </summary>
        /// <param name="menupont">the choosen menupont</param>
        /// <returns>False ha ki akarunk lépni</returns>
        public bool MuveletVegzes(int menupont)
        {
            switch (menupont)
            {
                case 11:
                    try
                    {
                        // int ID,string Nev,string OrszagNev, DateTime AlapitasEve, int EvesForgalom
                        Console.WriteLine("Elem hozzáadása az autómárka táblához.");
                        Console.WriteLine("Adja meg az autómárkát: ");
                        string nev = Console.ReadLine();
                        Console.WriteLine("Adja meg az országot: ");
                        string orszagnev = Console.ReadLine();
                        Console.WriteLine("Adja meg az alapítási évet: (yyyy-MM-dd)");
                        DateTime alapitasiEv = DateTime.Parse(Console.ReadLine());
                        Console.WriteLine("Adja meg az éves forgalmat");
                        int evesForgalom = int.Parse(Console.ReadLine());
                        this.dbLogic.AddAutomarka(nev, orszagnev, alapitasiEv, evesForgalom);
                        Console.WriteLine("Sikeres hozzáadás!");
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine("Sikertelen hozzáadás! " + e);
                    }

                    break;
                case 12:
                    try
                    {
                        // int ID, int marka_id, DateTime megjelenes_napja, int motorterfogat, int loero, int alapar, string nev
                        Console.WriteLine("Elem hozzáadása a modellek táblához.");
                        Console.WriteLine("Adja meg a modell márkáját: ");
                        string marka = Console.ReadLine();
                        int marka_id = this.dbLogic.AutomarkaKeresNevSzerint(marka);
                        Console.WriteLine("Adja meg a modell nevét: ");
                        string nev = Console.ReadLine();
                        Console.WriteLine("Adja meg a megjelenés napját: ");
                        DateTime megjelnes_napja = DateTime.Parse(Console.ReadLine());
                        Console.WriteLine("Adja meg a motor térfogatot: ");
                        int motorterfogat = int.Parse(Console.ReadLine());
                        Console.WriteLine("Adja meg a lóerőt: ");
                        int loero = int.Parse(Console.ReadLine());
                        Console.WriteLine("Adja meg az alapárat: ");
                        int alapar = int.Parse(Console.ReadLine());
                        this.dbLogic.AddModell(marka_id, megjelnes_napja, motorterfogat, loero, alapar, nev);
                        Console.WriteLine("Sikeres hozzáadás!");
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine("Sikertelen hozzáadás! " + e);
                    }

                    break;
                case 13:
                    try
                    {
                        // int ID, string kategorianev, string nev, int ar, string szin, bool tobbszor_hasznalhato_e
                        Console.WriteLine("Elem hozzáadása a extrák táblához.");
                        Console.WriteLine("Adja meg az extra felszerelés kategóriáját ");
                        string kategorianev = Console.ReadLine();
                        Console.WriteLine("Adja meg az extra felszerelés nevét: ");
                        string nev = Console.ReadLine();
                        Console.WriteLine("Adja meg az extra felszerelés árát: ");
                        int ar = int.Parse(Console.ReadLine());
                        Console.WriteLine("Adja meg az extra felszerelés színét: ");
                        string szin = Console.ReadLine();
                        Console.WriteLine("Többször használható az extra felszerelés?(igen/nem) ");
                        string the = Console.ReadLine();
                        bool tobbszor_hasznalhato_e = the == "igen" ? true : false;
                        this.dbLogic.AddExtra(kategorianev, nev, ar, szin, tobbszor_hasznalhato_e);
                        Console.WriteLine("Sikeres hozzáadás!");
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine("Sikertelen hozzáadás! " + e);
                    }

                    break;
                case 21:
                    try
                    {
                        // int ID, string kategorianev, string nev, int ar, string szin, bool tobbszor_hasznalhato_e
                        Console.WriteLine("Elem törlése az autómárka táblából ");
                        Console.WriteLine("Adja meg a törlenddő automárka nevét");
                        string automarka = Console.ReadLine();
                        this.dbLogic.RemoveAutomarka(this.dbLogic.AutomarkaKeresNevSzerint(automarka));
                        Console.WriteLine("Sikeres törlés!");
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine("Sikertelen törlés! " + e);
                    }

                    break;
                case 22:
                    try
                    {
                        // int ID, string kategorianev, string nev, int ar, string szin, bool tobbszor_hasznalhato_e
                        Console.WriteLine("Elem törlése az modellek táblából ");
                        Console.WriteLine("Adja meg a törlenddő modell ID-jét");
                        int modell_id = int.Parse(Console.ReadLine());
                        this.dbLogic.RemoveModell(modell_id);
                        Console.WriteLine("Sikeres törlés!");
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine("Sikertelen törlés! " + e);
                    }

                    break;
                case 23:
                    try
                    {
                        // int ID, string kategorianev, string nev, int ar, string szin, bool tobbszor_hasznalhato_e
                        Console.WriteLine("Elem törlése az extrák táblából ");
                        Console.WriteLine("Adja meg a törlenddő extra ID-jét");
                        int extra_id = int.Parse(Console.ReadLine());
                        this.dbLogic.RemoveModell(extra_id);
                        Console.WriteLine("Sikeres törlés!");
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine("Sikertelen törlés! " + e);
                    }

                    break;
                case 31:
                    try
                    {
                        Console.WriteLine("Elem frissítése az autómárka táblában ");
                        Console.WriteLine("Adja meg a frissítendő automárka nevét");
                        string automarka = Console.ReadLine();
                        int id = this.dbLogic.AutomarkaKeresNevSzerint(automarka);
                        Console.WriteLine("Adja meg az országot: ");
                        string orszagnev = Console.ReadLine();
                        Console.WriteLine("Adja meg az alapítási évet: (yyyy-MM-dd)");
                        DateTime alapitasiEv = DateTime.Parse(Console.ReadLine());
                        Console.WriteLine("Adja meg az éves forgalmat");
                        int evesForgalom = int.Parse(Console.ReadLine());
                        this.dbLogic.FrissitAutomarka(id, automarka, orszagnev, alapitasiEv, evesForgalom);
                        Console.WriteLine("Sikeres frissítés!");
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine("Sikertelen törlés! " + e);
                    }

                    break;
                case 32:
                    try
                    {
                        Console.WriteLine("Elem frissítése a modellek táblában ");
                        Console.WriteLine("Adja meg a frissítendő modell ID-jét");
                        int id = int.Parse(Console.ReadLine());
                        Console.WriteLine("Adja meg a márkát: ");
                        string marka = Console.ReadLine();
                        int marka_id = this.dbLogic.AutomarkaKeresNevSzerint(marka);
                        Console.WriteLine("Adja meg a modell nevét: ");
                        string modell_nev = Console.ReadLine();
                        Console.WriteLine("Adja meg a megjelenés napját: (yyyy-MM-dd)");
                        DateTime megjelenes_napja = DateTime.Parse(Console.ReadLine());
                        Console.WriteLine("Adja meg a motortérfogatot");
                        int motorterfogat = int.Parse(Console.ReadLine());
                        Console.WriteLine("Adja meg a lóerőt");
                        int loero = int.Parse(Console.ReadLine());
                        Console.WriteLine("Adja meg az alapárat");
                        int alapar = int.Parse(Console.ReadLine());
                        this.dbLogic.FrissitModell(id, marka_id, megjelenes_napja, motorterfogat, loero, alapar, modell_nev);
                        Console.WriteLine("Sikeres frissítés!");
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine("Sikertelen törlés! " + e);
                    }

                    break;
                case 33:
                    try
                    {
                        Console.WriteLine("Elem frissítése az extrák táblában ");
                        Console.WriteLine("Adja meg a frissítendő extra ID-jét");
                        int id = int.Parse(Console.ReadLine());
                        Console.WriteLine("Adja meg a kategóriát: ");
                        string kategorianev = Console.ReadLine();
                        Console.WriteLine("Adja meg az extra nevét: ");
                        string nev = Console.ReadLine();
                        Console.WriteLine("Adja meg az extra árát: ");
                        int ar = int.Parse(Console.ReadLine());
                        Console.WriteLine("Adja meg az extra színét: ");
                        string szin = Console.ReadLine();
                        Console.WriteLine("Többször használható az extra felszerelés?(igen/nem) ");
                        string the = Console.ReadLine();
                        bool tobbszor_hasznalhato_e = the == "igen" ? true : false;
                        this.dbLogic.FrissitExtra(id, kategorianev, nev, ar, szin, tobbszor_hasznalhato_e);
                        Console.WriteLine("Sikeres frissítés!");
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine("Sikertelen törlés! " + e);
                    }

                    break;

                case 41:
                    try
                    {
                        // int ID, string kategorianev, string nev, int ar, string szin, bool tobbszor_hasznalhato_e
                        Console.WriteLine("Autómárka tábla tartalma: ");
                        Console.WriteLine(this.dbLogic.AutomarkaTartalom);
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine("Sikertelen listázás! " + e);
                    }

                    break;
                case 42:
                    try
                    {
                        // int ID, string kategorianev, string nev, int ar, string szin, bool tobbszor_hasznalhato_e
                        Console.WriteLine("Modellek tábla tartalma: ");
                        Console.WriteLine(this.dbLogic.ModellTartalom);
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine("Sikertelen listázás! " + e);
                    }

                    break;
                case 43:
                    try
                    {
                        // int ID, string kategorianev, string nev, int ar, string szin, bool tobbszor_hasznalhato_e
                        Console.WriteLine("Extrák tábla tartalma: ");
                        Console.WriteLine(this.dbLogic.ExtraTartalom);
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine("Sikertelen listázás! " + e);
                    }

                    break;
                case 51:
                    Console.WriteLine("Az  1990 alapított márkák modelljei :");
                    foreach (var item in this.dbLogic.CarsAfter1990())
                    {
                        Console.WriteLine(item);
                    }

                    break;
                case 52:
                    Console.WriteLine("Modellek teljes ára :");
                    foreach (var item in this.dbLogic.CarFullPrice())
                    {
                        Console.WriteLine(item);
                    }

                    break;
                case 53:
                    Console.WriteLine("Minden német márka és model :");
                    foreach (var item in this.dbLogic.NemetAutomarkaESModell())
                    {
                        Console.WriteLine(item);
                    }

                    break;
                default:
                    return false;
            }

            Console.ReadLine();
            return true;
        }

        /// <summary>
        /// Kiírja a menusort
        /// </summary>
        /// <returns>menupont száma</returns>
        public int Menusor()
        {
            int menupont;
            int almenupont;
            int valasztas = 0;
            bool fomenu = true;
            bool almenu = true;
            while (fomenu)
            {
                Console.WriteLine("1, Elem hozzáadása megadott táblához");
                Console.WriteLine("2, Elem törlése megadott táblából");
                Console.WriteLine("3, Elem frissítése megadott táblából");
                Console.WriteLine("4, Elemek listázása megadott táblából");
                Console.WriteLine("5, egyéb műveletek");
                if (int.TryParse(Console.ReadLine(), out menupont))
                {
                    switch (menupont)
                    {
                        case 1:
                            while (almenu)
                            {
                                Console.WriteLine("Melyik táblához szeretne új elemet hozzáadni?");
                                Console.WriteLine("1, Automárkák");
                                Console.WriteLine("2, Modellek");
                                Console.WriteLine("3, Extrák");
                                if (int.TryParse(Console.ReadLine(), out almenupont))
                                {
                                    switch (almenupont)
                                    {
                                        case 1:
                                            valasztas = 11;
                                            almenu = false;
                                            break;
                                        case 2:
                                            valasztas = 12;
                                            almenu = false;
                                            break;
                                        case 3:
                                            valasztas = 13;
                                            almenu = false;
                                            break;
                                        default:
                                            Console.WriteLine("Érvénytlen menüpont! Kérem válasszon újra!");
                                            Console.ReadLine();
                                            break;
                                    }
                                }
                                else
                                {
                                    Console.WriteLine("Érvénytlen menüpont! Kérem válasszon újra!");
                                    Console.ReadLine();
                                }
                            }

                            fomenu = false;
                            break;
                        case 2:
                            while (almenu)
                            {
                                Console.WriteLine("Melyik táblából szeretne törölni?");
                                Console.WriteLine("1, Automárkák");
                                Console.WriteLine("2, Modellek");
                                Console.WriteLine("3, Extrák");
                                if (int.TryParse(Console.ReadLine(), out almenupont))
                                {
                                    switch (almenupont)
                                    {
                                        case 1:
                                            valasztas = 21;
                                            almenu = false;
                                            break;
                                        case 2:
                                            valasztas = 22;
                                            almenu = false;
                                            break;
                                        case 3:
                                            valasztas = 23;
                                            almenu = false;
                                            break;
                                        default:
                                            Console.WriteLine("Érvénytlen menüpont! Kérem válasszon újra!");
                                            Console.ReadLine();
                                            break;
                                    }
                                }
                                else
                                {
                                    Console.WriteLine("Érvénytlen menüpont! Kérem válasszon újra!");
                                    Console.ReadLine();
                                }
                            }

                            fomenu = false;
                            break;
                        case 3:
                            while (almenu)
                            {
                                Console.WriteLine("Melyik táblában szeretne frissíteni?");
                                Console.WriteLine("1, Automárkák");
                                Console.WriteLine("2, Modellek");
                                Console.WriteLine("3, Extrák");
                                if (int.TryParse(Console.ReadLine(), out almenupont))
                                {
                                    switch (almenupont)
                                    {
                                        case 1:
                                            valasztas = 31;
                                            almenu = false;
                                            break;
                                        case 2:
                                            valasztas = 32;
                                            almenu = false;
                                            break;
                                        case 3:
                                            valasztas = 33;
                                            almenu = false;
                                            break;
                                        default:
                                            Console.WriteLine("Érvénytlen menüpont! Kérem válasszon újra!");
                                            Console.ReadLine();
                                            break;
                                    }
                                }
                                else
                                {
                                    Console.WriteLine("Érvénytlen menüpont! Kérem válasszon újra!");
                                    Console.ReadLine();
                                }
                            }

                            fomenu = false;
                            break;
                        case 4:
                            while (almenu)
                            {
                                Console.WriteLine("Melyik táblát szeretné kilistázni?");
                                Console.WriteLine("1, Automárkák");
                                Console.WriteLine("2, Modellek");
                                Console.WriteLine("3, Extrák");
                                if (int.TryParse(Console.ReadLine(), out almenupont))
                                {
                                    switch (almenupont)
                                    {
                                        case 1:
                                            valasztas = 41;
                                            almenu = false;
                                            break;
                                        case 2:
                                            valasztas = 42;
                                            almenu = false;
                                            break;
                                        case 3:
                                            valasztas = 43;
                                            almenu = false;
                                            break;
                                        default:
                                            Console.WriteLine("Érvénytlen menüpont! Kérem válasszon újra!");
                                            Console.ReadLine();
                                            break;
                                    }
                                }
                                else
                                {
                                    Console.WriteLine("Érvénytlen menüpont! Kérem válasszon újra!");
                                    Console.ReadLine();
                                }
                            }

                            fomenu = false;
                            break;
                        case 5:
                            Console.WriteLine("1, 1990 után alapított márkák modelljei");
                            Console.WriteLine("2, Minden modell teljes ára(alapár + extrák ára)");
                            Console.WriteLine("3, A német automárák és modelljeik");
                            if (int.TryParse(Console.ReadLine(), out almenupont))
                            {
                                switch (almenupont)
                                {
                                    case 1:
                                        valasztas = 51;
                                        almenu = false;
                                        break;
                                    case 2:
                                        valasztas = 52;
                                        almenu = false;
                                        break;
                                    case 3:
                                        valasztas = 53;
                                        almenu = false;
                                        break;
                                    default:
                                        Console.WriteLine("Érvénytlen menüpont! Kérem válasszon újra!");
                                        Console.ReadLine();
                                        break;
                                }
                            }
                            else
                            {
                                Console.WriteLine("Érvénytlen menüpont! Kérem válasszon újra!");
                                Console.ReadLine();
                            }

                            fomenu = false;
                            break;
                        default:
                            Console.WriteLine("Érvénytlen menüpont! Kérem válasszon újra!");
                            Console.ReadLine();
                            break;
                    }
                }
                else
                {
                    Console.WriteLine("Ki szeretne lépni? (igen/nem)");
                    string valasz = Console.ReadLine();
                    if (valasz == "igen")
                    {
                        fomenu = false;
                        valasztas = 0;
                    }
                    else
                    {
                        Console.WriteLine("Folytatjuk, üssön egy entert");
                        Console.ReadLine();
                        Console.Clear();
                    }
                }
            }

            return valasztas;
        }
    }
}
