﻿// <copyright file="InditoProgram.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace CarShop.Program
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using CarShop.Logic;

    /// <summary>
    /// Főprogram
    /// </summary>
    public static class InditoProgram
    {
        /// <summary>
        /// Főprogram main metódja
        /// </summary>
        public static void Main()
        {
            bool fut = true;
            while (fut)
            {
                Menu menu = new Menu();
                fut = menu.MuveletVegzes(menu.Menusor());
            }
        }
    }
}
