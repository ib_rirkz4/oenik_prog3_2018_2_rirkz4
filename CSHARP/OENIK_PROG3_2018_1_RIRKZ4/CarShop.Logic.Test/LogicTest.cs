﻿// <copyright file="LogicTest.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace CarShop.Logic.Test
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using CarShop.Data;
    using CarShop.Logic;
    using CarShop.Repository;
    using Moq;
    using NUnit.Framework;

    /// <summary>
    /// The test logic
    /// </summary>
    [TestFixture]
    public class LogicTest
    {
        /// <summary>
        /// test
        /// </summary>
        [Test]
        public void InsertAutomarkaTest()
        {
            BusinessLogic m = Mock.Of<BusinessLogic>();
            Mock<BusinessLogic> logicMock = Mock.Get(m);
            List<automarka> lista = new List<automarka>() { new automarka() { AlapitasEve = DateTime.Parse("1994-09-09"), EvesForgalom = 50, OrszagNev = "CUCC", Nev = "CUCCLI" } };

            logicMock.Setup(x => x.Osszefogo.AR.All).Returns(lista.AsQueryable());
            logicMock.Object.AddAutomarka("ciabébi", "xd", DateTime.Parse("1994-09-09"), 200000);
            logicMock.Verify(mock => mock.AddAutomarka("ciabébi", "xd", DateTime.Parse("1994-09-09"), 200000), Times.Once());
        }

        /// <summary>
        /// test
        /// </summary>
        [Test]
        [Sequential]
        public void InsertModellTest()
        {
            BusinessLogic m = Mock.Of<BusinessLogic>();
            Mock<BusinessLogic> logicMock = Mock.Get(m);
            List<modellek> lista = new List<modellek>() { new modellek() { marka_id = 1, megjelenes_napja = DateTime.Parse("1994-09-09"), alapar = 250, loero = 120, motorterfogat = 120, név = "Jani" } };

            logicMock.Setup(x => x.Osszefogo.MR.All).Returns(lista.AsQueryable());
            logicMock.Object.AddModell(1, DateTime.Parse("1994-09-09"), 120, 120, 120, "Nincs");
            logicMock.Verify(mock => mock.AddModell(1, DateTime.Parse("1994-09-09"), 120, 120, 120, "Nincs"), Times.Once());
        }

        /// <summary>
        /// test
        /// </summary>
        [Test]
        [Sequential]
        public void InsertExtraTest()
        {
            BusinessLogic m = Mock.Of<BusinessLogic>();
            Mock<BusinessLogic> logicMock = Mock.Get(m);
            List<extrak> lista = new List<extrak>() { new extrak() { kategorianev = "semmi", nev = "Semmiseg", ar = 0, szin = "fehér", tobbszor_hasznalt_e = true } };

            logicMock.Setup(x => x.Osszefogo.ER.All).Returns(lista.AsQueryable());
            logicMock.Object.AddExtra("semmisem", "fura", 0, "fekete", true);
            logicMock.Verify(mock => mock.AddExtra("semmisem", "fura", 0, "fekete", true), Times.Once());
        }

        /// <summary>
        /// test
        /// </summary>
        [Test]
        [Sequential]
        public void InserKapcsoloTest()
        {
            BusinessLogic m = Mock.Of<BusinessLogic>();
            Mock<BusinessLogic> logicMock = Mock.Get(m);
            List<modellExtraKapcsolo> lista = new List<modellExtraKapcsolo>() { new modellExtraKapcsolo { modell_id = 1, extra_id = 1 } };

            logicMock.Setup(x => x.Osszefogo.Mekr.All).Returns(lista.AsQueryable());
            logicMock.Object.AddKapcsolo(2, 2);
            logicMock.Verify(mock => mock.AddKapcsolo(2, 2), Times.Once());
        }

        /// <summary>
        /// test
        /// </summary>
        [Test]
        [Sequential]
        public void UpdateAutomarkaTest()
        {
            BusinessLogic m = Mock.Of<BusinessLogic>();
            Mock<BusinessLogic> logicMock = Mock.Get(m);
            List<automarka> lista = new List<automarka>();
            lista.Add(new automarka() { Id = 1, AlapitasEve = DateTime.Parse("1994-09-09"), EvesForgalom = 50, OrszagNev = "CUCC", Nev = "CUCCLI2" });
            logicMock.Setup(x => x.Osszefogo.AR.All).Returns(lista.AsQueryable());
            Assert.That(logicMock.Object.FrissitAutomarka(1, "TESZT", "nincs", DateTime.Parse("1994-09-09"), 250), Is.False);
        }

        /// <summary>
        /// test
        /// </summary>
        [Test]
        public void UpdateModellTest()
        {
            BusinessLogic m = Mock.Of<BusinessLogic>();
            Mock<BusinessLogic> logicMock = Mock.Get(m);
            List<modellek> lista = new List<modellek>();
            lista.Add(new modellek() { Id = 1, marka_id = 1, megjelenes_napja = DateTime.Parse("1994-09-09"), motorterfogat = 100, loero = 100, alapar = 40, név = "cucc" });
            logicMock.Setup(x => x.Osszefogo.MR.All).Returns(lista.AsQueryable());
            Assert.That(logicMock.Object.FrissitModell(1, 1, DateTime.Parse("1994-09-09"), 100, 100, 40, "TESZT"), Is.False);
        }

        /// <summary>
        /// test
        /// </summary>
        [Test]
        public void UpdateExtraTest()
        {
            BusinessLogic m = Mock.Of<BusinessLogic>();
            Mock<BusinessLogic> logicMock = Mock.Get(m);
            List<modellek> lista = new List<modellek>();
            lista.Add(new modellek() { Id = 1, marka_id = 1, megjelenes_napja = DateTime.Parse("1994-09-09"), motorterfogat = 100, loero = 100, alapar = 40, név = "cucc" });
            logicMock.Setup(x => x.Osszefogo.MR.All).Returns(lista.AsQueryable());
            Assert.That(logicMock.Object.FrissitModell(1, 1, DateTime.Parse("1994-09-09"), 100, 100, 40, "TESZT"), Is.False);
        }

        /// <summary>
        /// test
        /// </summary>
        [Test]
        public void UpdateKapcsoloTest()
        {
            BusinessLogic m = Mock.Of<BusinessLogic>();
            Mock<BusinessLogic> logicMock = Mock.Get(m);
            List<modellek> lista = new List<modellek>();
            lista.Add(new modellek() { Id = 1, marka_id = 1, megjelenes_napja = DateTime.Parse("1994-09-09"), motorterfogat = 100, loero = 100, alapar = 40, név = "cucc" });
            logicMock.Setup(x => x.Osszefogo.MR.All).Returns(lista.AsQueryable());
            Assert.That(logicMock.Object.FrissitModell(1, 1, DateTime.Parse("1994-09-09"), 100, 100, 40, "TESZT"), Is.False);
        }

        /// <summary>
        /// test
        /// </summary>
        [Test]
        public void DeleteAutomarkaTest()
        {
            BusinessLogic m = Mock.Of<BusinessLogic>();
            Mock<BusinessLogic> logicMock = Mock.Get(m);
            List<automarka> lista = new List<automarka>() { new automarka() { AlapitasEve = DateTime.Parse("1994-09-09"), EvesForgalom = 50, OrszagNev = "CUCC", Nev = "CUCCLI" } };
            logicMock.Setup(x => x.Osszefogo.AR.All).Returns(lista.AsQueryable());
            Assert.That(logicMock.Object.RemoveAutomarka(1), Is.False);
        }

        /// <summary>
        /// test
        /// </summary>
        [Test]
        public void DeleteModellkaTest()
        {
            BusinessLogic m = Mock.Of<BusinessLogic>();
            Mock<BusinessLogic> logicMock = Mock.Get(m);
            List<modellek> lista = new List<modellek>();
            lista.Add(new modellek() { Id = 1, marka_id = 1, megjelenes_napja = DateTime.Parse("1994-09-09"), motorterfogat = 100, loero = 100, alapar = 40, név = "cucc" });
            logicMock.Setup(x => x.Osszefogo.MR.All).Returns(lista.AsQueryable());
            Assert.That(logicMock.Object.RemoveModell(1), Is.False);
        }

        /// <summary>
        /// test
        /// </summary>
        [Test]
        public void DeleteExtraTest()
        {
            BusinessLogic m = Mock.Of<BusinessLogic>();
            Mock<BusinessLogic> logicMock = Mock.Get(m);
            List<extrak> lista = new List<extrak>() { new extrak() { kategorianev = "semmi", nev = "Semmiseg", ar = 0, szin = "fehér", tobbszor_hasznalt_e = true } };

            logicMock.Setup(x => x.Osszefogo.ER.All).Returns(lista.AsQueryable());
            Assert.That(logicMock.Object.RemoveExtra(1), Is.False);
        }

        /// <summary>
        /// test
        /// </summary>
        [Test]
        public void DeleteKapcsoloTest()
        {
            BusinessLogic m = Mock.Of<BusinessLogic>();
            Mock<BusinessLogic> logicMock = Mock.Get(m);
            List<modellExtraKapcsolo> lista = new List<modellExtraKapcsolo>() { new modellExtraKapcsolo { modell_id = 1, extra_id = 1 } };

            logicMock.Setup(x => x.Osszefogo.Mekr.All).Returns(lista.AsQueryable());
            Assert.That(logicMock.Object.RemoveKapcsolo(1), Is.False);
        }

        /// <summary>
        /// test
        /// </summary>
        [Test]
        public void ReadAutomarkaTest()
        {
            BusinessLogic m = Mock.Of<BusinessLogic>();
            Mock<BusinessLogic> logicMock = Mock.Get(m);
            List<automarka> lista = new List<automarka>() { new automarka() { AlapitasEve = DateTime.Parse("1994-09-09"), EvesForgalom = 50, OrszagNev = "CUCC", Nev = "CUCCLI" } };
            logicMock.Setup(x => x.Osszefogo.AR.All).Returns(lista.AsQueryable());
            List<automarka> list2 = logicMock.Object.Osszefogo.AR.All.ToList();
            Assert.That(list2.Count > 0);
        }

        /// <summary>
        /// test
        /// </summary>
        [Test]
        public void ReadModellTest()
        {
            BusinessLogic m = Mock.Of<BusinessLogic>();
            Mock<BusinessLogic> logicMock = Mock.Get(m);
            List<modellek> lista = new List<modellek>() { new modellek() { marka_id = 1, megjelenes_napja = DateTime.Parse("1994-09-09"), alapar = 250, loero = 120, motorterfogat = 120, név = "Jani" } };

            logicMock.Setup(x => x.Osszefogo.MR.All).Returns(lista.AsQueryable());
            List<modellek> list2 = logicMock.Object.Osszefogo.MR.All.ToList();
            Assert.That(list2.Count > 0);
        }

        /// <summary>
        /// test
        /// </summary>
        [Test]
        public void ReadExtraTest()
        {
            BusinessLogic m = Mock.Of<BusinessLogic>();
            Mock<BusinessLogic> logicMock = Mock.Get(m);
            List<extrak> lista = new List<extrak>() { new extrak() { kategorianev = "semmi", nev = "Semmiseg", ar = 0, szin = "fehér", tobbszor_hasznalt_e = true } };

            logicMock.Setup(x => x.Osszefogo.ER.All).Returns(lista.AsQueryable());
            List<extrak> list2 = logicMock.Object.Osszefogo.ER.All.ToList();
            Assert.That(list2.Count > 0);
        }

        /// <summary>
        /// test
        /// </summary>
        [Test]
        public void ReadKapcsoloTest()
        {
            BusinessLogic m = Mock.Of<BusinessLogic>();
            Mock<BusinessLogic> logicMock = Mock.Get(m);
            List<modellExtraKapcsolo> lista = new List<modellExtraKapcsolo>() { new modellExtraKapcsolo { modell_id = 1, extra_id = 1 } };

            logicMock.Setup(x => x.Osszefogo.Mekr.All).Returns(lista.AsQueryable());
            List<modellExtraKapcsolo> list2 = logicMock.Object.Osszefogo.Mekr.All.ToList();
            Assert.That(list2.Count > 0);
        }

        /// <summary>
        /// test
        /// </summary>
        [Test]
        public void CarsAfter1990()
        {
            BusinessLogic m = Mock.Of<BusinessLogic>();
            Mock<BusinessLogic> logicMock = Mock.Get(m);
            List<automarka> lista = new List<automarka>() { new automarka() { AlapitasEve = DateTime.Parse("1994-09-09"), EvesForgalom = 50, OrszagNev = "CUCC", Nev = "CUCCLI" } };
            List<modellek> lista2 = new List<modellek>() { new modellek() { marka_id = 1, megjelenes_napja = DateTime.Parse("1994-09-09"), alapar = 250, loero = 120, motorterfogat = 120, név = "Jani" } };
            logicMock.Setup(x => x.Osszefogo.AR.All).Returns(lista.AsQueryable());
            logicMock.Setup(x => x.Osszefogo.MR.All).Returns(lista2.AsQueryable());
            List<string> kocsi = logicMock.Object.CarsAfter1990();
            Assert.That(kocsi.Count() > 0);
        }

        /// <summary>
        /// test
        /// </summary>
        [Test]
        public void CarFullPrice()
        {
            BusinessLogic m = Mock.Of<BusinessLogic>();
            Mock<BusinessLogic> logicMock = Mock.Get(m);
            List<automarka> lista = new List<automarka>() { new automarka() { AlapitasEve = DateTime.Parse("1994-09-09"), EvesForgalom = 50, OrszagNev = "CUCC", Nev = "CUCCLI" } };
            List<modellek> lista2 = new List<modellek>() { new modellek() { marka_id = 1, megjelenes_napja = DateTime.Parse("1994-09-09"), alapar = 250, loero = 120, motorterfogat = 120, név = "Jani" } };
            List<extrak> lista3 = new List<extrak>() { new extrak() { kategorianev = "semmi", nev = "Semmiseg", ar = 0, szin = "fehér", tobbszor_hasznalt_e = true } };
            List<modellExtraKapcsolo> lista4 = new List<modellExtraKapcsolo>() { new modellExtraKapcsolo { modell_id = 1, extra_id = 1 } };
            logicMock.Setup(x => x.Osszefogo.AR.All).Returns(lista.AsQueryable());
            logicMock.Setup(x => x.Osszefogo.MR.All).Returns(lista2.AsQueryable());
            logicMock.Setup(x => x.Osszefogo.ER.All).Returns(lista3.AsQueryable());
            logicMock.Setup(x => x.Osszefogo.Mekr.All).Returns(lista4.AsQueryable());
            List<string> kocsi = logicMock.Object.CarFullPrice();
            Assert.That(kocsi.Count() > 0);
        }

        /// <summary>
        /// test
        /// </summary>
        [Test]
        public void NemetKocsik()
        {
            BusinessLogic m = Mock.Of<BusinessLogic>();
            Mock<BusinessLogic> logicMock = Mock.Get(m);
            List<automarka> lista = new List<automarka>() { new automarka() { AlapitasEve = DateTime.Parse("1994-09-09"), EvesForgalom = 50, OrszagNev = "CUCC", Nev = "CUCCLI" } };
            List<modellek> lista2 = new List<modellek>() { new modellek() { marka_id = 1, megjelenes_napja = DateTime.Parse("1994-09-09"), alapar = 250, loero = 120, motorterfogat = 120, név = "Jani" } };
            List<extrak> lista3 = new List<extrak>() { new extrak() { kategorianev = "semmi", nev = "Semmiseg", ar = 0, szin = "fehér", tobbszor_hasznalt_e = true } };
            List<modellExtraKapcsolo> lista4 = new List<modellExtraKapcsolo>() { new modellExtraKapcsolo { modell_id = 1, extra_id = 1 } };
            logicMock.Setup(x => x.Osszefogo.AR.All).Returns(lista.AsQueryable());
            logicMock.Setup(x => x.Osszefogo.MR.All).Returns(lista2.AsQueryable());
            logicMock.Setup(x => x.Osszefogo.ER.All).Returns(lista3.AsQueryable());
            logicMock.Setup(x => x.Osszefogo.Mekr.All).Returns(lista4.AsQueryable());
            List<string> kocsi = logicMock.Object.NemetAutomarkaESModell();
            Assert.That(kocsi.Count > 0);
        }

        /// <summary>
        /// test
        /// </summary>
        [Test]
        public void KocsiLetezik()
        {
            BusinessLogic m = Mock.Of<BusinessLogic>();
            Mock<BusinessLogic> logicMock = Mock.Get(m);
            List<automarka> lista = new List<automarka>() { new automarka() { AlapitasEve = DateTime.Parse("1994-09-09"), EvesForgalom = 50, OrszagNev = "CUCC", Nev = "CUCCLI" } };
            logicMock.Setup(x => x.Osszefogo.AR.All).Returns(lista.AsQueryable());
            bool letezik = logicMock.Object.LetezikEAutomarka(1);
            Assert.That(letezik == true);
        }

        /// <summary>
        /// test
        /// </summary>
        [Test]
        public void ModellLetezik()
        {
            BusinessLogic m = Mock.Of<BusinessLogic>();
            Mock<BusinessLogic> logicMock = Mock.Get(m);
            List<modellek> lista2 = new List<modellek>() { new modellek() { marka_id = 1, megjelenes_napja = DateTime.Parse("1994-09-09"), alapar = 250, loero = 120, motorterfogat = 120, név = "Jani" } };
            logicMock.Setup(x => x.Osszefogo.MR.All).Returns(lista2.AsQueryable());
            bool letezik = logicMock.Object.LetezikEModell(1);
            Assert.That(letezik == true);
        }

        /// <summary>
        /// test
        /// </summary>
        [Test]
        public void ExtraLetezik()
        {
            BusinessLogic m = Mock.Of<BusinessLogic>();
            Mock<BusinessLogic> logicMock = Mock.Get(m);
            List<extrak> lista3 = new List<extrak>() { new extrak() { kategorianev = "semmi", nev = "Semmiseg", ar = 0, szin = "fehér", tobbszor_hasznalt_e = true } };
            logicMock.Setup(x => x.Osszefogo.ER.All).Returns(lista3.AsQueryable());
            bool letezik = logicMock.Object.LetezikEExtra(1);
            Assert.That(letezik == true);
        }

        /// <summary>
        /// test
        /// </summary>
        [Test]
        public void AutoKeresNevszerint()
        {
            BusinessLogic m = Mock.Of<BusinessLogic>();
            Mock<BusinessLogic> logicMock = Mock.Get(m);
            List<automarka> lista = new List<automarka>() { new automarka() { Id = 1, AlapitasEve = DateTime.Parse("1994-09-09"), EvesForgalom = 50, OrszagNev = "CUCC", Nev = "CUCCLI" } };
            logicMock.Setup(x => x.Osszefogo.AR.All).Returns(lista.AsQueryable());
            int id = logicMock.Object.AutomarkaKeresNevSzerint("CUCCLI");
            Assert.That(id, Is.EqualTo(0));
        }
    }
}
