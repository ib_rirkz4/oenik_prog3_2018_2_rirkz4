var hierarchy =
[
    [ "CarShop.Data.AdatbazisElero", "class_car_shop_1_1_data_1_1_adatbazis_elero.html", null ],
    [ "CarShop.Data.automarka", "class_car_shop_1_1_data_1_1automarka.html", null ],
    [ "CarShop.Repository.Test.Class1", "class_car_shop_1_1_repository_1_1_test_1_1_class1.html", null ],
    [ "DbContext", null, [
      [ "CarShop.Data.CarShopDataBaseEntities", "class_car_shop_1_1_data_1_1_car_shop_data_base_entities.html", null ]
    ] ],
    [ "CarShop.Data.extrak", "class_car_shop_1_1_data_1_1extrak.html", null ],
    [ "CarShop.Logic.ILogic", "interface_car_shop_1_1_logic_1_1_i_logic.html", [
      [ "CarShop.Logic.BusinessLogic", "class_car_shop_1_1_logic_1_1_business_logic.html", null ]
    ] ],
    [ "CarShop.Program.InditoProgram", "class_car_shop_1_1_program_1_1_indito_program.html", null ],
    [ "CarShop.Repository.IRepository< T >", "interface_car_shop_1_1_repository_1_1_i_repository.html", null ],
    [ "CarShop.Repository.IRepository< automarka >", "interface_car_shop_1_1_repository_1_1_i_repository.html", [
      [ "CarShop.Repository.AutomarkaRepository", "class_car_shop_1_1_repository_1_1_automarka_repository.html", null ]
    ] ],
    [ "CarShop.Repository.IRepository< extrak >", "interface_car_shop_1_1_repository_1_1_i_repository.html", [
      [ "CarShop.Repository.ExtrakRepository", "class_car_shop_1_1_repository_1_1_extrak_repository.html", null ]
    ] ],
    [ "CarShop.Repository.IRepository< modellek >", "interface_car_shop_1_1_repository_1_1_i_repository.html", [
      [ "CarShop.Repository.ModellekRepository", "class_car_shop_1_1_repository_1_1_modellek_repository.html", null ]
    ] ],
    [ "CarShop.Repository.IRepository< modellExtraKapcsolo >", "interface_car_shop_1_1_repository_1_1_i_repository.html", [
      [ "CarShop.Repository.ModellExtraKapcsoloRepository", "class_car_shop_1_1_repository_1_1_modell_extra_kapcsolo_repository.html", null ]
    ] ],
    [ "CarShop.Logic.Test.LogicTest", "class_car_shop_1_1_logic_1_1_test_1_1_logic_test.html", null ],
    [ "CarShop.Program.Menu", "class_car_shop_1_1_program_1_1_menu.html", null ],
    [ "CarShop.Data.modellek", "class_car_shop_1_1_data_1_1modellek.html", null ],
    [ "CarShop.Data.modellExtraKapcsolo", "class_car_shop_1_1_data_1_1modell_extra_kapcsolo.html", null ],
    [ "CarShop.Repository.OsszefogoRepository", "class_car_shop_1_1_repository_1_1_osszefogo_repository.html", null ]
];