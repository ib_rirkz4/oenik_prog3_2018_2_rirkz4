var namespace_car_shop_1_1_repository =
[
    [ "Test", "namespace_car_shop_1_1_repository_1_1_test.html", "namespace_car_shop_1_1_repository_1_1_test" ],
    [ "AutomarkaRepository", "class_car_shop_1_1_repository_1_1_automarka_repository.html", "class_car_shop_1_1_repository_1_1_automarka_repository" ],
    [ "ExtrakRepository", "class_car_shop_1_1_repository_1_1_extrak_repository.html", "class_car_shop_1_1_repository_1_1_extrak_repository" ],
    [ "IRepository", "interface_car_shop_1_1_repository_1_1_i_repository.html", "interface_car_shop_1_1_repository_1_1_i_repository" ],
    [ "ModellekRepository", "class_car_shop_1_1_repository_1_1_modellek_repository.html", "class_car_shop_1_1_repository_1_1_modellek_repository" ],
    [ "ModellExtraKapcsoloRepository", "class_car_shop_1_1_repository_1_1_modell_extra_kapcsolo_repository.html", "class_car_shop_1_1_repository_1_1_modell_extra_kapcsolo_repository" ],
    [ "OsszefogoRepository", "class_car_shop_1_1_repository_1_1_osszefogo_repository.html", "class_car_shop_1_1_repository_1_1_osszefogo_repository" ]
];