var searchData=
[
  ['addautomarka',['AddAutomarka',['../class_car_shop_1_1_data_1_1_adatbazis_elero.html#a4e20e8a145ff816e442c1c986fa510ae',1,'CarShop.Data.AdatbazisElero.AddAutomarka()'],['../class_car_shop_1_1_logic_1_1_business_logic.html#a790621a52ca15297561e4ee43914f692',1,'CarShop.Logic.BusinessLogic.AddAutomarka()'],['../interface_car_shop_1_1_logic_1_1_i_logic.html#a6f6c7ac07b7f785fc14dbb081e3b57a5',1,'CarShop.Logic.ILogic.AddAutomarka()']]],
  ['addextra',['AddExtra',['../class_car_shop_1_1_data_1_1_adatbazis_elero.html#a09e0ddda4e61fef98d86ec56a01e25ce',1,'CarShop.Data.AdatbazisElero.AddExtra()'],['../class_car_shop_1_1_logic_1_1_business_logic.html#ae2fb68eb3ebca80a98fa6d9bb48174ae',1,'CarShop.Logic.BusinessLogic.AddExtra()'],['../interface_car_shop_1_1_logic_1_1_i_logic.html#aac2bbdeb0976f6eed82d74003f74bae6',1,'CarShop.Logic.ILogic.AddExtra()']]],
  ['addextrakapcsolo',['AddExtraKapcsolo',['../class_car_shop_1_1_data_1_1_adatbazis_elero.html#af6b9e037598921e066a2ccaa88d22bd7',1,'CarShop::Data::AdatbazisElero']]],
  ['addkapcsolo',['AddKapcsolo',['../class_car_shop_1_1_logic_1_1_business_logic.html#a49dc0ce541f365c94460d8c79ac46446',1,'CarShop.Logic.BusinessLogic.AddKapcsolo()'],['../interface_car_shop_1_1_logic_1_1_i_logic.html#ab68b96148db48cd45b977f5c6b7f8572',1,'CarShop.Logic.ILogic.AddKapcsolo()']]],
  ['addmarka',['AddMarka',['../class_car_shop_1_1_data_1_1_adatbazis_elero.html#a3380c779dc054405cb25cdba0804a37a',1,'CarShop::Data::AdatbazisElero']]],
  ['addmodell',['AddModell',['../class_car_shop_1_1_logic_1_1_business_logic.html#a783f0528d4a6bd9192e0c5c0139d8a89',1,'CarShop.Logic.BusinessLogic.AddModell()'],['../interface_car_shop_1_1_logic_1_1_i_logic.html#a2e76b095c30a1bb53e2e8749d7453246',1,'CarShop.Logic.ILogic.AddModell()']]],
  ['autokeresnevszerint',['AutoKeresNevszerint',['../class_car_shop_1_1_logic_1_1_test_1_1_logic_test.html#aaf624ba8ca170abdc2cb15df2270898d',1,'CarShop::Logic::Test::LogicTest']]],
  ['automarka',['automarka',['../class_car_shop_1_1_data_1_1automarka.html#ac43f382c517d6bbc5e7b509c0100d371',1,'CarShop::Data::automarka']]],
  ['automarkakeresnevszerint',['AutomarkaKeresNevSzerint',['../class_car_shop_1_1_logic_1_1_business_logic.html#acb84f39f48337f0d5143071da191d135',1,'CarShop::Logic::BusinessLogic']]],
  ['automarkaletezike',['AutomarkaLetezikE',['../class_car_shop_1_1_repository_1_1_automarka_repository.html#ad6c6593bedf14bbbb3ed4eede5d2854a',1,'CarShop::Repository::AutomarkaRepository']]],
  ['automarkarepository',['AutomarkaRepository',['../class_car_shop_1_1_repository_1_1_automarka_repository.html#a902b976757fc6635558708bc5dfc6112',1,'CarShop::Repository::AutomarkaRepository']]]
];
