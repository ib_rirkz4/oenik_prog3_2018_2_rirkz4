var searchData=
[
  ['id',['Id',['../class_car_shop_1_1_data_1_1automarka.html#a2296963072ed198046e2344dd3939a8b',1,'CarShop::Data::automarka']]],
  ['ilogic',['ILogic',['../interface_car_shop_1_1_logic_1_1_i_logic.html',1,'CarShop::Logic']]],
  ['inditoprogram',['InditoProgram',['../class_car_shop_1_1_program_1_1_indito_program.html',1,'CarShop::Program']]],
  ['inserkapcsolotest',['InserKapcsoloTest',['../class_car_shop_1_1_logic_1_1_test_1_1_logic_test.html#afc426e4e9da5f00d7f0f0dff3cbead9c',1,'CarShop::Logic::Test::LogicTest']]],
  ['insertautomarkatest',['InsertAutomarkaTest',['../class_car_shop_1_1_logic_1_1_test_1_1_logic_test.html#a3efa0bef286d75e872278ba2cf4cae42',1,'CarShop::Logic::Test::LogicTest']]],
  ['insertextratest',['InsertExtraTest',['../class_car_shop_1_1_logic_1_1_test_1_1_logic_test.html#a134dcfb6d6dd249d00e4c793b2e9ccde',1,'CarShop::Logic::Test::LogicTest']]],
  ['insertmodelltest',['InsertModellTest',['../class_car_shop_1_1_logic_1_1_test_1_1_logic_test.html#a638a8393b133dfdab665b077b43fe47a',1,'CarShop::Logic::Test::LogicTest']]],
  ['irepository',['IRepository',['../interface_car_shop_1_1_repository_1_1_i_repository.html',1,'CarShop::Repository']]],
  ['irepository_3c_20automarka_20_3e',['IRepository&lt; automarka &gt;',['../interface_car_shop_1_1_repository_1_1_i_repository.html',1,'CarShop::Repository']]],
  ['irepository_3c_20extrak_20_3e',['IRepository&lt; extrak &gt;',['../interface_car_shop_1_1_repository_1_1_i_repository.html',1,'CarShop::Repository']]],
  ['irepository_3c_20modellek_20_3e',['IRepository&lt; modellek &gt;',['../interface_car_shop_1_1_repository_1_1_i_repository.html',1,'CarShop::Repository']]],
  ['irepository_3c_20modellextrakapcsolo_20_3e',['IRepository&lt; modellExtraKapcsolo &gt;',['../interface_car_shop_1_1_repository_1_1_i_repository.html',1,'CarShop::Repository']]]
];
