var class_car_shop_1_1_logic_1_1_test_1_1_logic_test =
[
    [ "AutoKeresNevszerint", "class_car_shop_1_1_logic_1_1_test_1_1_logic_test.html#aaf624ba8ca170abdc2cb15df2270898d", null ],
    [ "CarFullPrice", "class_car_shop_1_1_logic_1_1_test_1_1_logic_test.html#a23f0eac2828fadd69497d05bd2b86437", null ],
    [ "CarsAfter1990", "class_car_shop_1_1_logic_1_1_test_1_1_logic_test.html#a00584f61d3a345069ee3a00e39c7f2f7", null ],
    [ "DeleteAutomarkaTest", "class_car_shop_1_1_logic_1_1_test_1_1_logic_test.html#a266197ccc33f2ea0a828265b359ff22b", null ],
    [ "DeleteExtraTest", "class_car_shop_1_1_logic_1_1_test_1_1_logic_test.html#aa3948d22c571cde280639e17ef460ebf", null ],
    [ "DeleteKapcsoloTest", "class_car_shop_1_1_logic_1_1_test_1_1_logic_test.html#aaf148ba07066bea035594d65d63a264c", null ],
    [ "DeleteModellkaTest", "class_car_shop_1_1_logic_1_1_test_1_1_logic_test.html#ab5af7cf05971d923a9bd103d4cce1456", null ],
    [ "ExtraLetezik", "class_car_shop_1_1_logic_1_1_test_1_1_logic_test.html#a164a6c95e5c4c42cf457fd123d377935", null ],
    [ "InserKapcsoloTest", "class_car_shop_1_1_logic_1_1_test_1_1_logic_test.html#afc426e4e9da5f00d7f0f0dff3cbead9c", null ],
    [ "InsertAutomarkaTest", "class_car_shop_1_1_logic_1_1_test_1_1_logic_test.html#a3efa0bef286d75e872278ba2cf4cae42", null ],
    [ "InsertExtraTest", "class_car_shop_1_1_logic_1_1_test_1_1_logic_test.html#a134dcfb6d6dd249d00e4c793b2e9ccde", null ],
    [ "InsertModellTest", "class_car_shop_1_1_logic_1_1_test_1_1_logic_test.html#a638a8393b133dfdab665b077b43fe47a", null ],
    [ "KocsiLetezik", "class_car_shop_1_1_logic_1_1_test_1_1_logic_test.html#a2cb3f28725c819f1462e1e9572dc5808", null ],
    [ "ModellLetezik", "class_car_shop_1_1_logic_1_1_test_1_1_logic_test.html#a0751ecb825ca2bde067927bfce188bc1", null ],
    [ "NemetKocsik", "class_car_shop_1_1_logic_1_1_test_1_1_logic_test.html#a97f11e1153442c4bf88cfe8767dda1c2", null ],
    [ "ReadAutomarkaTest", "class_car_shop_1_1_logic_1_1_test_1_1_logic_test.html#aeafd192e00b26ed0f57e7375384b5636", null ],
    [ "ReadExtraTest", "class_car_shop_1_1_logic_1_1_test_1_1_logic_test.html#a0814a3c58674faca02d2d3271155ff86", null ],
    [ "ReadKapcsoloTest", "class_car_shop_1_1_logic_1_1_test_1_1_logic_test.html#a7d65b469c43ccab0665ac7ff67b6900d", null ],
    [ "ReadModellTest", "class_car_shop_1_1_logic_1_1_test_1_1_logic_test.html#a9c4ebd73e84c332da325221ffb2f4dad", null ],
    [ "UpdateAutomarkaTest", "class_car_shop_1_1_logic_1_1_test_1_1_logic_test.html#a94a61658afbf408324ba4e04f3846943", null ],
    [ "UpdateExtraTest", "class_car_shop_1_1_logic_1_1_test_1_1_logic_test.html#a9c3841c30013e365e3fa8be3d727550d", null ],
    [ "UpdateKapcsoloTest", "class_car_shop_1_1_logic_1_1_test_1_1_logic_test.html#ad3183df76ce7cf67e53e3614e5927d14", null ],
    [ "UpdateModellTest", "class_car_shop_1_1_logic_1_1_test_1_1_logic_test.html#a6beb245aef0a9a74431706484fb5fa51", null ]
];