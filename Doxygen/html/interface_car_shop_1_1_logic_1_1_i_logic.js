var interface_car_shop_1_1_logic_1_1_i_logic =
[
    [ "AddAutomarka", "interface_car_shop_1_1_logic_1_1_i_logic.html#a6f6c7ac07b7f785fc14dbb081e3b57a5", null ],
    [ "AddExtra", "interface_car_shop_1_1_logic_1_1_i_logic.html#aac2bbdeb0976f6eed82d74003f74bae6", null ],
    [ "AddKapcsolo", "interface_car_shop_1_1_logic_1_1_i_logic.html#ab68b96148db48cd45b977f5c6b7f8572", null ],
    [ "AddModell", "interface_car_shop_1_1_logic_1_1_i_logic.html#a2e76b095c30a1bb53e2e8749d7453246", null ],
    [ "FrissitAutomarka", "interface_car_shop_1_1_logic_1_1_i_logic.html#a24587eb6cb7721dabc6b4aaa9da8877a", null ],
    [ "FrissitExtra", "interface_car_shop_1_1_logic_1_1_i_logic.html#a9175f03264a2700a48551fd140061d39", null ],
    [ "FrissitKapcsolo", "interface_car_shop_1_1_logic_1_1_i_logic.html#a7dcbe68caf6793078df53067c7086e57", null ],
    [ "FrissitModell", "interface_car_shop_1_1_logic_1_1_i_logic.html#ab3337391e933399a58b7639f6f7ea118", null ],
    [ "LetezikEAutomarka", "interface_car_shop_1_1_logic_1_1_i_logic.html#a2a017bff515d0202d5505cf786076498", null ],
    [ "LetezikEExtra", "interface_car_shop_1_1_logic_1_1_i_logic.html#a1f694ed9a2774b86002b4c1710f8c95c", null ],
    [ "LetezikEModell", "interface_car_shop_1_1_logic_1_1_i_logic.html#a9b14f4955f71d46c214baad551274f86", null ],
    [ "MindenTabla", "interface_car_shop_1_1_logic_1_1_i_logic.html#ab333690ff05f70ddeb94f791f1bad456", null ],
    [ "RemoveAutomarka", "interface_car_shop_1_1_logic_1_1_i_logic.html#a18eff06f4d27dd6c94cbc5e9f8ecc06c", null ],
    [ "RemoveExtra", "interface_car_shop_1_1_logic_1_1_i_logic.html#a11903f7c4083593f82ae2f25f0f180b7", null ],
    [ "RemoveKapcsolo", "interface_car_shop_1_1_logic_1_1_i_logic.html#a8adb84437a38a63d28146156f77a524b", null ],
    [ "RemoveModell", "interface_car_shop_1_1_logic_1_1_i_logic.html#a2172420939ca4bc511a26f0d949ed4d8", null ],
    [ "AutomarkaTartalom", "interface_car_shop_1_1_logic_1_1_i_logic.html#a59ea9878ab0216474a551cce804f0870", null ],
    [ "ExtraTartalom", "interface_car_shop_1_1_logic_1_1_i_logic.html#a5fa88cff49459ab54282c653c070e859", null ],
    [ "Kapcsolotartalom", "interface_car_shop_1_1_logic_1_1_i_logic.html#a3e03e3f2b36e72642fb13ff9dda52102", null ],
    [ "ModellTartalom", "interface_car_shop_1_1_logic_1_1_i_logic.html#a0b242ee792038ad0348b082f2065bc0f", null ]
];