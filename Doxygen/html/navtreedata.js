/*
@ @licstart  The following is the entire license notice for the
JavaScript code in this file.

Copyright (C) 1997-2017 by Dimitri van Heesch

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

@licend  The above is the entire license notice
for the JavaScript code in this file
*/
var NAVTREE =
[
  [ "Beadandosírás", "index.html", [
    [ "Castle Core Changelog", "md__c_1__users_benja__documents_oenik_prog3_2018_1_rirkz4__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2018fa8d914985712fdd7642f965716b62cd.html", null ],
    [ "NUnit 3.11 - October 11, 2018", "md__c_1__users_benja__documents_oenik_prog3_2018_1_rirkz4__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_20189d518c9e49dd3c0a3c3a1afe018d1699.html", null ],
    [ "OENIK_PROG3_2018_1_RIRKZ4", "md__c_1__users_benja__documents_oenik_prog3_2018_1_rirkz4__c_s_h_a_r_p__r_e_a_d_m_e.html", null ],
    [ "OENIK_PROG3_2018_1_RIRKZ4", "md__c_1__users_benja__documents_oenik_prog3_2018_1_rirkz4__j_a_v_a__r_e_a_d_m_e.html", null ],
    [ "OENIK_PROG3_2018_1_RIRKZ4", "md__c_1__users_benja__documents_oenik_prog3_2018_1_rirkz4__r_e_a_d_m_e.html", null ],
    [ "Packages", "namespaces.html", [
      [ "Packages", "namespaces.html", "namespaces_dup" ]
    ] ],
    [ "Classes", "annotated.html", [
      [ "Class List", "annotated.html", "annotated_dup" ],
      [ "Class Index", "classes.html", null ],
      [ "Class Hierarchy", "hierarchy.html", "hierarchy" ],
      [ "Class Members", "functions.html", [
        [ "All", "functions.html", null ],
        [ "Functions", "functions_func.html", null ],
        [ "Variables", "functions_vars.html", null ],
        [ "Properties", "functions_prop.html", null ]
      ] ]
    ] ],
    [ "Files", "files.html", [
      [ "File List", "files.html", "files_dup" ]
    ] ]
  ] ]
];

var NAVTREEINDEX =
[
"_adatbazis_elero_8cs_source.html",
"interface_car_shop_1_1_logic_1_1_i_logic.html#aac2bbdeb0976f6eed82d74003f74bae6"
];

var SYNCONMSG = 'click to disable panel synchronisation';
var SYNCOFFMSG = 'click to enable panel synchronisation';