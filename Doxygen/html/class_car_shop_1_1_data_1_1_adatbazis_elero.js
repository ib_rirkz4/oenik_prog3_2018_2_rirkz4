var class_car_shop_1_1_data_1_1_adatbazis_elero =
[
    [ "AddAutomarka", "class_car_shop_1_1_data_1_1_adatbazis_elero.html#a4e20e8a145ff816e442c1c986fa510ae", null ],
    [ "AddExtra", "class_car_shop_1_1_data_1_1_adatbazis_elero.html#a09e0ddda4e61fef98d86ec56a01e25ce", null ],
    [ "AddExtraKapcsolo", "class_car_shop_1_1_data_1_1_adatbazis_elero.html#af6b9e037598921e066a2ccaa88d22bd7", null ],
    [ "AddMarka", "class_car_shop_1_1_data_1_1_adatbazis_elero.html#a3380c779dc054405cb25cdba0804a37a", null ],
    [ "DeleteAutomarka", "class_car_shop_1_1_data_1_1_adatbazis_elero.html#adb53e576f4de3d1574e64c331b655bc3", null ],
    [ "DeleteExtra", "class_car_shop_1_1_data_1_1_adatbazis_elero.html#ad83e690acea6d630ba94a4b4c6f2b7f9", null ],
    [ "DeleteExtraKapcsolo", "class_car_shop_1_1_data_1_1_adatbazis_elero.html#abb5741a43ce15b12c1d1dd1226b8c6ea", null ],
    [ "DeleteModell", "class_car_shop_1_1_data_1_1_adatbazis_elero.html#a3abbf876488d2e69333c2cacc51ce423", null ],
    [ "SaveChanges", "class_car_shop_1_1_data_1_1_adatbazis_elero.html#aebc67a3a636ade9ba492c9f879ac82a8", null ],
    [ "Automarkak", "class_car_shop_1_1_data_1_1_adatbazis_elero.html#a81f32a8b41cdc3f20d1120213e5d688c", null ],
    [ "CarShopAllomanyok", "class_car_shop_1_1_data_1_1_adatbazis_elero.html#af9fee0d05b66cdfed47e96da4421fff8", null ],
    [ "Extrak", "class_car_shop_1_1_data_1_1_adatbazis_elero.html#a4e99aa7ad135970eb2f6233e85fedb11", null ],
    [ "Modellek", "class_car_shop_1_1_data_1_1_adatbazis_elero.html#ac107dfe2166a9a22a971a3a8d3a1892a", null ],
    [ "ModellekExtraKapcsolo", "class_car_shop_1_1_data_1_1_adatbazis_elero.html#ae0294b8420ceb1045a1b472eca786a4c", null ],
    [ "TablaNevek", "class_car_shop_1_1_data_1_1_adatbazis_elero.html#a64adc5aae0e0b1ff3177e35369c549d6", null ]
];